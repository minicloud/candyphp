<?php
/***
 * Candy框架 框架APP应用自动创建类
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2019-07-27 12:55:58 $   
 */

declare (strict_types = 1);
namespace Candy\Core;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');

final Class Build {
	private static $mess = [];    //提示消息
	
	/*
	 * 创建文件
	 *
	 * @param	string	$fileName	需要创建的文件名
	 * @param	string	$str		需要向文件中写的内容字符串
	 */
	private static function touch(string $fileName,string $str): void
	{
		if(!fileExistsCase($fileName)){
			if(file_put_contents($fileName, $str)){
				self::$mess[]="创建文件 {$fileName} 成功.";
			}
		}
	}
	
	/*
	 * 创建目录
	 *
	 * @param	string	$dirs		需要创建的目录名称
	 */
	private static function mkdir(array $dirs): void
	{
		foreach($dirs as $dir){
			if(!is_dir($dir)){
				if(\Candy\Extend\Dir::create($dir)){
					self::$mess[]="创建目录 {$dir} 成功.";
				}
			}
		}
	}
	
	/*
	 * 创建基础配置文件
	 */
	public static function checkInit(): void
	{
		//第一次首先创建配置文件
		if(fileExistsCase(RUNTIME.'init.lock') === false){
			self::mkdir([CANDYROOT.'Application/',SOURCEPATH,SOURCEPATH.'Inc/']);
			//初始化文件
			self::touch(INCPATH.'Init.inc.php', base64_decode('PD9waHAKLy/ns7vnu5/liJ3lp4vljJbmlofku7YKZGVmaW5lZCgnQ0FORFlDTVMnKSBPUiBkZWZpbmUoJ0NBTkRZQ01TJywgdHJ1ZSk7CgovL+iwg+ivleaooeW8jwpkZWZpbmVkKCdERUJVRycpIE9SIGRlZmluZSgnREVCVUcnLCAxKTsKCi8v6buY6K6k6aG555uu5qih5p2/6Lev5b6ECiR0cGxwYXRoID0gJ0hvbWVALi9UZW1wbGF0ZSc7CgovL+iHquWumuS5iem7mOiupOaTjeS9nAovL0coJ2luZGV4SW5pdFByb3VybCcsIFsnYXBwJz0+J0FkbWluJywnY29udHJvbCc9PidMb2dpbicsJ2FjdGlvbic9Pidjb2RlJ10pOwo='));
		}
	}
	
	/**
	 * 创建系统运行时的文件
	 */
	private static function runtime(): void
	{
		//传参
		$appName = C('APPNAME');
		$tmpPath = C('TMPPATH');
		
		$dirs = [
				RUNTIME,   //系统的缓存目录
				RUNTIME.'Cache/',   //系统的缓存目录
				RUNTIME.'Comps/',   //模板的组合文件
				RUNTIME.'Data/cache',    //文件缓存数据
				RUNTIME.'Controls/',
				RUNTIME.'Models/',
				RUNTIME.'Logs/',
				RUNTIME.'Tmps/'
			];
		
		if(is_null(C('ISADDON'))){
			$dirs[] = RUNTIME.'Cache/'. $tmpPath;
			$dirs[] = RUNTIME.'Comps/'. $tmpPath .'/'. C('TPLSTYLE');
			$dirs[] = RUNTIME.'Controls/'. $tmpPath;
			$dirs[] = RUNTIME.'Models/'. $tmpPath;
		}else{
			$dirs[] = RUNTIME.'Cache/Addons/'. $appName;
			$dirs[] = RUNTIME.'Comps/Addons/'. $appName;
			$dirs[] = RUNTIME.'Controls/Addons/'. $appName;
			$dirs[] = RUNTIME.'Models/Addons/'. $appName;
		}
		
		self::mkdir($dirs);   //创建目录	
	}
		
	/**
	 *创建项目的目录结构
	 */
	public static function start(): void
	{
		//初始化runtime目录
		if(!is_dir(RUNTIME))
			self::runtime();
		
		//文件锁，一旦生成，就不再创建
		$structFile = RUNTIME.'init.lock';  //初始化锁
		
		//传参
		$appPath = C('APP_PATH');
		$appName = C('APPNAME');
		$tplPrefix = G('TPLPREFIX');
		$tplStyle = C('TPLSTYLE');
		$separate = G('SEPARATE');
		$app = C('APP');
		
		//自动创建APP目录 初次创建除外
		if((isset($_GET['add']) && (!is_dir($appPath) || ((isset($_GET['name']) || in_array($_GET['add'], ['', 'addon', 'plug'])))) && is_null(C('ISADDON'))) || (fileExistsCase($structFile) === false && $appName == 'Home')){
			if(!fileExistsCase($structFile)){
				//配置初始化
				$fileName = INCPATH.'Config.inc.php';
				if(!fileExistsCase($fileName)){
					//添加配置模板
					self::touch($fileName, base64_decode('PD9waHAKLy/phY3nva7kv6Hmga8KCnJldHVybiBbCgknQ0hBUlNFVCcJCT0+ICd1dGY4JywJLy/orr7nva7mlbDmja7lupPnmoTkvKDovpPlrZfnrKbpm4YKCSdEUklWRVInCQk9PiAncGRvJywJLy/mlbDmja7lupPnmoTpqbHliqgKCSdEQicJCQk9PiBbCgkJJ2RlZmF1bHQnID0+IFsKCQkJJ0hPU1QnID0+ICcnLAkJLy/mlbDmja7lupPkuLvmnLoKCQkJJ1VTRVInID0+ICcnLAkJLy/mlbDmja7lupPnlKjmiLflkI0KCQkJJ1BBU1MnID0+ICcnLAkJLy/mlbDmja7lupPlr4bnoIEKCQkJJ0RCTkFNRScgPT4gJycsCQkvL+aVsOaNruW6k+WQjeensAoJCQknVEFCUFJFRklYJyA9PiAnJwkvL+ihqOWJjee8gAoJCV0KCV0sCgknQ1NUQVJUJwkJPT4gMCwJLy/pobXpnaLnvJPlrZjlvIDlhbMKCSdDVElNRScJCQk9PiAzNjAwLAkvL+mhtemdoue8k+WtmOaXtumXtAoJJ0NUWVBFJwkJCT0+IDAsCS8v6aG16Z2i57yT5a2Y57G75Z6LCgknU2VydmVycycJPT4gWwoJCSd0eXBlJwkJPT4gJ01lbWNhY2hlZCcsCS8v57G75Z6LCgkJJ29wZW4nCQk9PiBmYWxzZSwJLy/kvb/nlKhtZW1jYWNoZeacjeWKoeWZqAoJCSdjb25maW5nJwk9PiBbJ2hvc3QnPT4nbG9jYWxob3N0JywgJ3BvcnQnPT4xMTIxMV0JLy/phY3nva4KCV0KCS8vJ1NlcnZlcnMnCT0+IFsKCS8vCSd0eXBlJwkJPT4gJ3JlZGlzJywJLy/nsbvlnosKCS8vCSdvcGVuJwkJPT4gZmFsc2UsCS8v5L2/55SoUmVkaXPmnI3liqHlmagKCS8vCSdjb25maW5nJwk9PiBbJ2hvc3QnPT4nbG9jYWxob3N0JywgJ3BvcnQnPT42Mzc5LCdwYXNzd29yZCc9PicnXQkvL+mFjee9rgoJLy9dCl07Cg=='));
				}
				
				//扩展目录初始化
				$dirs = [
					SOURCEPATH . 'Vendor/',   	//项目引用类库目录
					SOURCEPATH . 'Class/',    //项目的通用类
					SOURCEPATH . 'Func/',    //项目的通用函数库
					SOURCEPATH . 'Common/',    //项目基类
					CANDYROOT . 'Public/uploads/',  //系统公共上传文件目录
					CANDYROOT . 'Public/css/',      //系统公css共目录
					CANDYROOT . 'Public/js/',       //系统公共javascript目录
					CANDYROOT . 'Public/images/',   //系统公共图片目录
					CANDYROOT . 'Public/thumbs/',	//系统缩略图目录
				];
				self::mkdir($dirs);
				
				//复制demo示例
				\Candy\Extend\Dir::copyDir(CANDYPATH.'Tpl/demo/Hook', CANDYROOT.'Plugins/Demo/Hook');
				copy(CANDYPATH.'Tpl/demo/conf.php', CANDYROOT.'Plugins/Demo/conf.php');
				
				//添加公共404
				self::touch(CANDYROOT.'Public/404'. $tplPrefix, 'PCFET0NUWVBFIGh0bWw+PGh0bWw+PGhlYWQ+PG1ldGEgY2hhcnNldD0idWZ0LTgiPjx0aXRsZT40MDQvcGFnZSBub3QgZm91bmQuPC90aXRsZT48bWV0YSBodHRwLWVxdWl2PSJYLVVBLUNvbXBhdGlibGUiIGNvbnRlbnQ9IklFPWVkZ2UsY2hyb21lPTEiPjxtZXRhIG5hbWU9InZpZXdwb3J0IiBjb250ZW50PSJ3aWR0aD1kZXZpY2Utd2lkdGgsIGluaXRpYWwtc2NhbGU9MSwgbWF4aW11bS1zY2FsZT0xIj48bWV0YSBuYW1lPSJhcHBsZS1tb2JpbGUtd2ViLWFwcC1zdGF0dXMtYmFyLXN0eWxlIiBjb250ZW50PSJibGFjayI+PG1ldGEgbmFtZT0iYXBwbGUtbW9iaWxlLXdlYi1hcHAtY2FwYWJsZSIgY29udGVudD0ieWVzIj48c3R5bGU+aHRtbCwgYm9keSB7IG1hcmdpbjogMDsgZm9udC1mYW1pbHk6ICJBcmlhbCIsICJNaWNyb3NvZnQgWWFIZWkiLCAi6buR5L2TIiwgIuWui+S9kyIsIHNhbnMtc2VyaWY7IGZvbnQtc2l6ZTogMTRweDt9LmNvbnRlbnQgeyB3aWR0aDogMzVyZW07IG1hcmdpbjogMCBhdXRvOyBkaXNwbGF5OiBibG9jazsgbWFyZ2luLXRvcDogMTAwcHg7fS5jb250ZW50LWZvZiB7IHdpZHRoOiAxMDAlOyBmb250LXNpemU6IDEwcmVtOyBjb2xvcjogI0I0QkNDQzsgdGV4dC1hbGlnbjogY2VudGVyO30uY29udGVudC1kaXZpZGUgeyB3aWR0aDogMTAwJTsgbWFyZ2luLXRvcDogMTBweDsgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNFREYyRkM7fS5jb250ZW50LW1zZyB7IHdpZHRoOiAxMDAlOyB0ZXh0LWFsaWduOiBjZW50ZXI7IGZvbnQtc2l6ZTogMS4zcmVtOyBjb2xvcjogI0Q4RENFNTsgbWFyZ2luLXRvcDogMjBweDsgZm9udC13ZWlnaHQ6IDMwMDt9PC9zdHlsZT48L2hlYWQ+PGJvZHk+PGRpdiBjbGFzcz0iY29udGVudCI+PGRpdiBjbGFzcz0iY29udGVudC1mb2YiPjQwNDwvZGl2PjxkaXYgY2xhc3M9ImNvbnRlbnQtZGl2aWRlIj48L2Rpdj48ZGl2IGNsYXNzPSJjb250ZW50LW1zZyI+cGFnZSBub3QgZm91bmQuPC9kaXY+PC9kaXY+PC9ib2R5PjwvaHRtbD4=');
				
				//配置信息
				$incInfo = base64_decode('PD9waHAKLyoqCiAq5omp5bGV6YWN572uCiAqKi8KcmV0dXJuIFsKCSdBRERPTl9VUERBVEUnID0+CTAsCQkJCQkvL+abtOaWsOaXtumXtAoJJ0FERE9OX0xJU1QnID0+IFsJCQkJLy/mj5Lku7booagKCQknRGVtbycgPT4gWwoJCQknZW5hYmxlJwkJID0+IDAsCgkJCSdpbnN0YWxsZWQnCQkgPT4gMCwKCQkJJ2FkZG9uaWQnCQkgPT4gMTAKCQldLAoJXSwKXTsK');
				
				//配置文件
				self::touch(INCPATH.'Plug.inc.php', str_replace('ADDON', 'PLUG', $incInfo));
				
				if(is_null($app)){
					$addondirs = [
						CANDYROOT . 'Addons/Demo/',
						RUNTIME . 'Cache/Addons/',   //系统的缓存目录
						RUNTIME . 'Comps/Addons/',   //模板的组合文件
						RUNTIME . 'Controls/Addons/', 
						RUNTIME . 'Models/Addons/'
					];
					self::mkdir($addondirs);
					self::touch(INCPATH.'Addon.inc.php', $incInfo);
					
					//复制demo示例
					\Candy\Extend\Dir::copyDir(CANDYPATH.'Tpl/demo/Addon', CANDYROOT.'Addons/Demo/');
				}
				
				//全局路由
				if(is_null($app))
					$globRoute = INCPATH . 'Route.inc.php';
					self::touch($globRoute, base64_decode('PD9waHAKLyoqCiAq572R56uZ6Lev55Sx6YWN572uCiAqKi8KCnJldHVybiBbCgknVVJMX01PREVMJwkJCQk9PgkyLAkJCQkJLy9VUkzorr/pl67mqKHlvI8gICAwICDlhbzlrrnmqKHlvI8gIDEg57uf5LiA5qih5byPCgknVVJMX1JPVVRFUl9PTicgICAJCT0+IHRydWUsCQkJCS8v5byA5ZCv6Lev55SxCgkvLydVUkxfREVQUicgICAJCQk9PiAnLScsCQkJCQkvL+WIhumalOespgoJJ1VSTF9IVE1MX1NVRkZJWCcJCT0+Jy5zaHRtbCcsCQkJCS8v5Lyq6Z2Z5oCB5ZCO57yACgknVVJMX01BUF9SVUxFUycgCQk9PiBbIAkJCQkvL+mdmeaAgei3r+eUseinhOWImQoJCSdzaG93L2luZGV4JyAgICAgICAgPT4gJ2luZGV4L2luZGV4JywKCV0sCgknVVJMX1JPVVRFX1JVTEVTJyAJCT0+IFsgCQkJCS8v5Yqo5oCB6Lev55Sx6KeE5YiZCgkJJ3Nob3cvOmlkJyAgICAgICAgPT4gJ2luZGV4L2luZGV4JywKCV0sCl07'));
				
				//基础配置文件
				self::touch(INCPATH.'System.inc.php', base64_decode('PD9waHAKZGVmaW5lZCgnQ0FORFknKSBPUiBkaWUoJ1lvdSBBcmUgQSBCYWQgR3V5LiBvX08/Pz8nKTsKCi8v57O757uf5Z+656GA6YWN572u5paH5Lu2'));
				
				//扩展函数库初始化
				self::touch(SOURCEPATH.'Func/global.func.php', base64_decode('PD9waHAKZGVmaW5lZCgnQ0FORFknKSBPUiBkaWUoJ1lvdSBBcmUgQSBCYWQgR3V5LiBvX08/Pz8nKTsKCi8v5YWo5bGA5Y+v5Lul5L2/55So55qE6YCa55So5Ye95pWw5aOw5piO5Zyo6L+Z5Liq5paH5Lu25LitLgoKLyoqCiAqIOmihOe9rueahOaooeeJiOWkhOeQhuWHveaVsAogKgogKiBAcGFyYW0gc3RyaW5nICRwYXRoaW5mbwkvL3BhdGhpbmZvCiAqLwpmdW5jdGlvbiBiZWZvcmVDb250cmFzdFJvdXRlKCRwYXRoaW5mbykgewoJcmV0dXJuICRwYXRoaW5mbzsKfQoKLyoqCiAqIOmihOe9rueahOaooeeJiOWkhOeQhuWHveaVsAogKgogKiBAcGFyYW0gc3RyaW5nICRjb250ZW50CS8v5qih54mI6L6T5Ye65YaF5a65CiAqLwpmdW5jdGlvbiBiZWZvcmVIdG1sU2hvdygkY29udGVudCkgewoJcmV0dXJuICRjb250ZW50Owp9CgovKioKICog6aKE572u55qEVVJM6YWN572u5aSE55CG5Ye95pWwCiAqCiAqIEByZXR1cm4gYXJyYXkgJGNvbmZpZ3MJLy/phY3nva4KICovCmZ1bmN0aW9uIHNldFByb3VybENvbmZpZygpIHsKCXJldHVybiBhcnJheSgpOwp9'));
			
				//特殊路径类库
				self::touch(INCPATH.'Specialclass.inc.php', base64_decode('PD9waHAKZGVmaW5lZCgnQ0FORFknKSBPUiBkaWUoJ1lvdSBBcmUgQSBCYWQgR3V5LiBvX08/Pz8nKTsKCi8v54m55q6K57G75bqTCnJldHVybiBbXTs='));
			
				//特殊路径类库
				self::touch(INCPATH.'AttackCode.inc.php', base64_decode('PD9waHAKZGVmaW5lZCgnQ0FORFknKSBPUiBkaWUoJ1lvdSBBcmUgQSBCYWQgR3V5LiBvX08/Pz8nKTsKCi8vR0VU5ZKMUE9TVOmdnuazleWtl+espui/h+a7pOmFjee9ru+8iOmYsumdnuazleWtl+espuaUu+WHu++8iQpyZXR1cm4gWwoJLy9HRVTlgLzpnZ7ms5XlrZfnrKbov4fmu6QKCSdnZXQnICA9PiBbCgkJJ3NlbGVjdCAnLAoJCSdpbnNlcnQgJywKCQknXCcnLAoJCScvKicsCgkJJyonLAoJCScuLi8nLAoJCScuLlxcJywKCQkndW5pb24gJywKCQknaW50byAnLAoJCSdsb2FkX2ZpbGUoJywKCQknb3V0ZmlsZSAnLAoJCSc8c2NyaXB0JywKCV0sCgkvL1BPU1TlgLzpnZ7ms5XlrZfnrKbov4fmu6QKCSdwb3N0JyA9PiBbCgkJJzxzY3JpcHQnLAoJCSc8c3R5bGUnLAoJCSc8bWV0YScsCgldLApdOwo='));
				
				//命令行配置文件
				self::touch(INCPATH.'StandardCommand.inc.php', base64_decode('PD9waHAKZGVmaW5lZCgnQ0FORFknKSBPUiBkaWUoJ1lvdSBBcmUgQSBCYWQgR3V5LiBvX08/Pz8nKTsKCi8v5qGG5p625omp5bGV5ZG95LukCnJldHVybiBbCgkndGVzdCc9PmZ1bmN0aW9uICgkc3RyKXsKCQllY2hvICRzdHI7Cgl9Cl07'));
				
				//项目基类
				self::touch(SOURCEPATH.'Common/Base.class.php', base64_decode('PD9waHAKLyoqCiAqIOaJgOaciemhueebrueahOWfuuexuwogKiovCm5hbWVzcGFjZSBDYW5keVxBUFBcQ29tbW9uOwoKZGVmaW5lZCgnQ0FORFknKSBPUiBkaWUoJ1lvdSBBcmUgQSBCYWQgR3V5LiBvX08/Pz8nKTsKCkNsYXNzIEJhc2UgZXh0ZW5kcyBcQ2FuZHlcQ29yZVxBY3Rpb24gewoJCn0K'));
			}
			
			//创建项目或扩展
			isset($_GET['add']) OR $_GET['add'] = '';
			switch($_GET['add'])
			{
				case 'addon':
					if(is_null(G('Addons')))
						showTplMsg('halt', '项目未开启Addon功能。');
				
					$addonName = is_null($app) ? $appName : (isset($_GET['name']) ? $_GET['name'] : '');
					if(empty($addonName))
						showTplMsg('halt', 'Addon的名称不能为空。');
					
					$addonName = ucfirst(strtolower($addonName));
					
					//防止重新创建
					if(is_dir(CANDYROOT . 'Addons/'. $addonName .'/')){
						showTplMsg('halt', '该扩展已存在。');
					}
					
					$addondirs = [
						CANDYROOT . 'Addons/'. $addonName .'/'
					];
					self::mkdir($addondirs);
					\Candy\Extend\Dir::copyDir(CANDYPATH.'Tpl/demo/Addon', CANDYROOT.'Addons/'. $addonName .'/');
					
					//替换命名空间
					file_put_contents(CANDYROOT.'Addons/'. $addonName .'/Controls/Common.class.php', str_replace('APP\\Addon\\Demo', 'APP\\Addon\\'.$addonName, file_get_contents(CANDYROOT.'Addons/'. $addonName .'/Controls/Common.class.php')));
					file_put_contents(CANDYROOT.'Addons/'. $addonName .'/Controls/Index.class.php', str_replace('APP\\Addon\\Demo', 'APP\\Addon\\'.$addonName, file_get_contents(CANDYROOT.'Addons/'. $addonName .'/Controls/Index.class.php')));
					break;
				case 'plug':
					$plugName = isset($_GET['name']) ? $_GET['name'] : '';
					if(empty($plugName))
						showTplMsg('halt', 'Plugin的名称不能为空。');
					
					$plugName = ucfirst(strtolower($plugName));
					
					//防止重新创建
					if(is_dir(CANDYROOT . 'Plugins/'. $plugName .'/')){
						showTplMsg('halt', '该插件已存在。');
					}
					
					//插件目录
					$dirs = [
						CANDYROOT . 'Plugins/'. $plugName .'/Controls/Home/',
						CANDYROOT . 'Plugins/'. $plugName .'/Languages/Home/',
						CANDYROOT . 'Plugins/'. $plugName .'/Models/Home/',
						CANDYROOT . 'Plugins/'. $plugName .'/Views/Home/' . $tplStyle .'/Resource/css/',
						CANDYROOT . 'Plugins/'. $plugName .'/Views/Home/' . $tplStyle .'/Resource/js/',
						CANDYROOT . 'Plugins/'. $plugName .'/Views/Home/' . $tplStyle .'/Resource/images/',
					];
					self::mkdir($dirs);
					\Candy\Extend\Dir::copyDir(CANDYPATH.'Tpl/demo/Hook', CANDYROOT.'Plugins/'. $plugName .'/Hook'); 
					copy(CANDYPATH.'Tpl/demo/conf.php', CANDYROOT.'Plugins/'. $plugName .'/conf.php');
					break;
				default :
					if(C('APP')){
						$appName = isset($_GET['name']) ? $_GET['name'] : '';
						if(empty($appName)){
							showTplMsg('halt', '单例模式下创建项目名称不能为空。');
						}
						$appPath = CANDYROOT.'Application/'. $appName .'/';
					}
					
					//防止重新创建
					if(is_dir($appPath)){
						showTplMsg('halt', '该项目已存在。');
					}
					
					//APP目录结构
					$appdirs = [
						$appPath,                   //当前的应用目录
						$appPath . 'Models/',         //当前应用的模型目录
						$appPath . 'Vendor/',         //当前应用的类库扩展目录
						$appPath . 'Controls/',       //当前应用的控制器目录
						$appPath . 'Languages/zh-cn/',      //当前应用的语言库目录
					];
					self::mkdir($appdirs);
					
					//扩展目录
					$plugdirs = [
						PLUGPATH . C('RAPPNAME') . '/Models/',
						PLUGPATH . C('RAPPNAME') . '/Controls/',
						PLUGPATH . C('RAPPNAME') . '/Languages/zh-cn/',
						PLUGPATH . C('RAPPNAME') . '/Views/' . $tplStyle .'/Resource/css/',
						PLUGPATH . C('RAPPNAME') . '/Views/' . $tplStyle .'/Resource/js/',
						PLUGPATH . C('RAPPNAME') . '/Views/' . $tplStyle .'/Resource/images/',
					];
					self::mkdir($plugdirs);
					\Candy\Extend\Dir::copyDir(CANDYPATH.'Tpl/demo/Hook', CANDYROOT.'Plugins/'. C('RAPPNAME') .'/Hook');
					
					$path = $appPath . 'Views/';
					$success = $appPath .'Views/'. $tplStyle . '/Public'. $separate .'success' . $tplPrefix;
					//自定义模板路径
					if(!is_null(C('APP_TPL_PATH'))){
						list($app, $tpl_path) = explode('@', C('APP_TPL_PATH'));
						if($appName == $app){
							$path = rtrim($tpl_path,'/') . '/';
							
							//基础模板
							$success = $path. $tplStyle . '/Public'. $separate .'success'. $tplPrefix;
						}
					}
					
					//模板目录
					$views = [
						$path. $tplStyle . '/Resource/css/',     //当前应用模板CSS目录
						$path. $tplStyle . '/Resource/js/',      //当前应用模板js目录
						$path. $tplStyle . '/Resource/images/'  //当前应用模板图标目录
					];
					
					if($separate == '/'){
						$views[] = $path. $tplStyle . '/Public/';
						$views[] = $path. $tplStyle . '/Index/';
					}
					
					//创建模板目录
					self::mkdir($views);		
					
					if($tplStyle == 'default'){
						$index = rtrim($path,'/') . '/'. $tplStyle . '/Index'. $separate .'index'. $tplPrefix;
						self::touch($index, str_replace('{APP}', $appName, base64_decode('PCFkb2N0eXBlIGh0bWw+PGh0bWw+PGhlYWQ+PHRpdGxlPuasoui/juS9v+eUqENhbmR577yBPC90aXRsZT48L2hlYWQ+PGJvZHk+PHN0eWxlIHR5cGU9InRleHQvY3NzIj4qeyBwYWRkaW5nOiAwOyBtYXJnaW46IDA7IH0gYm9keXsgaGVpZ2h0OiAxMDAlOyB3aWR0aDogMTAwJTsgYmFja2dyb3VuZC1jb2xvcjojZjNmNWZhOyBmb250LWZhbWlseTogIuW+rui9r+mbhem7kSI7IGNvbG9yOiAjMzMzO2ZvbnQtc2l6ZToyNHB4fSBoMXsgZm9udC1zaXplOiAxMDBweDsgZm9udC13ZWlnaHQ6IG5vcm1hbDsgbWFyZ2luLWJvdHRvbTogMTJweDsgfSBweyBsaW5lLWhlaWdodDogMS44ZW07IGZvbnQtc2l6ZTogMzZweCB9PC9zdHlsZT48ZGl2IHN0eWxlPSJwYWRkaW5nOiAyNHB4IDQ4cHg7Ij4gPGgxPjopPC9oMT48IS0te2hvb2sgZGVtby5odG1sfS0tPjxici8+WyDmgqjnjrDlnKjorr/pl67nmoTmmK97QVBQfemhueebrkluZGV45o6n5Yi25Zmo55qEaW5kZXjmqKHlnZcgXTwvZGl2PjwvYm9keT48L2h0bWw+')));
					}
					
					//创建统一的消息模板
					if(!fileExistsCase($success)){
						//创建模板文件
						copy(CANDYPATH.'Tpl/success.tpl', $success);
						chmod($success,0777);
						
						//添加模板404
						$tpl404 = $path. $tplStyle . '/Public'. $separate .'404'. $tplPrefix;
						self::touch($tpl404, 'This is 404 file.');
					}
					
					//项目基类初始化
					self::touch($appPath.'Controls/Common.class.php', str_replace('{APPNAME}', C('LAPPNAME'), base64_decode('PD9waHAKbmFtZXNwYWNlIEFQUFxDb250cm9sXHtBUFBOQU1FfTsKCmRlZmluZWQoJ0NBTkRZJykgT1IgZGllKCdZb3UgQXJlIEEgQmFkIEd1eS4gb19PPz8/Jyk7CgpDbGFzcyBDb21tb24gZXh0ZW5kcyBCYXNlIHsKCWZ1bmN0aW9uIGluaXQoKXsKCgl9CQkKfQ==')));
			
					//项目控制器初始化
					self::touch($appPath.'Controls/Index.class.php', str_replace('{APPNAME}', C('LAPPNAME'), base64_decode('PD9waHAKbmFtZXNwYWNlIEFQUFxDb250cm9sXHtBUFBOQU1FfTsKCmRlZmluZWQoJ0NBTkRZJykgT1IgZGllKCdZb3UgQXJlIEEgQmFkIEd1eS4gb19PPz8/Jyk7CgpDbGFzcyBJbmRleCB7CglmdW5jdGlvbiBpbmRleCgpewoJCS8vaG9vayBkZW1vLnBocAoJCSR0aGlzLT5kaXNwbGF5KCk7Cgl9CQp9')));
					
					//项目基础语言库初始化		
					self::touch($appPath.'Languages/zh-cn/Common.lang.php', base64_decode('PD9waHAKZGVmaW5lZCgnQ0FORFknKSBPUiBkaWUoJ1lvdSBBcmUgQSBCYWQgR3V5LiBvX08/Pz8nKTsKCi8v6aG555uu5YWs5YWx6K+t6KiA5bqT'));
					
					//项目操作语言库初始化
					self::touch($appPath.'Languages/zh-cn/Index.lang.php', base64_decode('PD9waHAKZGVmaW5lZCgnQ0FORFknKSBPUiBkaWUoJ1lvdSBBcmUgQSBCYWQgR3V5LiBvX08/Pz8nKTsKCi8v6aG555uu5pON5L2c6K+t6KiA5bqT'));
					
					//初始化路由
					$route = INCPATH . $appName . 'Route.inc.php';
					self::touch($route, base64_decode('PD9waHAKLyoqCiAqIOe9keermei3r+eUsemFjee9rgogKiovCgpyZXR1cm4gWwoJJ1VSTF9NT0RFTCcgPT4JMSwgLy9VUkzorr/pl67mqKHlvI8gMCDlhbzlrrnmqKHlvI8gMSDnu5/kuIDmqKHlvI8KCSdVUkxfUk9VVEVSX09OJyA9PiB0cnVlLCAvL+W8gOWQr+i3r+eUsQoJLy8nVVJMX0RFUFInID0+ICctJywgLy/liIbpmpTnrKYKCSdVUkxfSFRNTF9TVUZGSVgnID0+Jy5odG1sJywgLy/kvKrpnZnmgIHlkI7nvIAKCSdVUkxfTUFQX1JVTEVTJyA9PiBbIC8v6Z2Z5oCB6Lev55Sx6KeE5YiZCgkJJ3Nob3cvaW5kZXgnID0+ICdIb21lL0luZGV4L2luZGV4JywKCV0sCgknVVJMX1JPVVRFX1JVTEVTJyA9PiBbIC8v5Yqo5oCB6Lev55Sx6KeE5YiZCgkJJ3Nob3cvOmlkJyA9PiAnSG9tZS9pbmRleC9pbmRleCcsCgldLApdOwo='));
					
					//项目生成记录
					self::touch($structFile, implode(PHP_EOL, self::$mess));
			}
			
			if(fileExistsCase($structFile) === false){
				$mkmsg = '';
				foreach(self::$mess as $mess){
					$mkmsg .= $mess . "\n\t";
				}
				self::touch($structFile, $mkmsg);
			}
		}
	}
}
