<?php
namespace JsonRpc;
use Workerman\Worker;
use JsonRpc\Protocols\JsonNL;

/**
 * JsonRpc server.
 */
class RpcServer
{
    /**
     * Worker instance.
     * @var worker
     */
    protected $_worker = null;

    /**
     * All data.
     * @var array
     */
    protected $_dataArray = array();

    /**
     * Construct.
     * @param string $ip
     * @param int $port
     */
    public function __construct($ip = '0.0.0.0', $port = 2020, $count = 4)
    {
		if (!class_exists('\Protocols\JsonNL')) {
            class_alias('JsonRpc\Protocols\JsonNL', 'Protocols\JsonNL');
        }
        $worker = new Worker("JsonNL://$ip:$port");
        $worker->count = $count;
        $worker->name = 'JsonRpcServer';
        $worker->onMessage = array($this, 'onMessage');
        $worker->reloadable = false;
        $this->_worker = $worker; 
    }
    
    /**
     * onMessage.
     * @param TcpConnection $connection
     * @param string $buffer
     */
    public function onMessage($connection, $data)
    {
		if (empty($data)) {
			$error = ['code' => '-32700', 'message' => '语法解析错误', 'meaning' => '服务端接收到无效的JSON'];
            $response = ['jsonrpc' => '2.0', 'id' => $data['id'], 'result' => null, 'error' => $error];
		} elseif (!isset($data['id']) || !isset($data['class']) || !isset($data['method']) || !isset($data['params'])) {
            $error = ['code' => '-32600', 'message' => '无效的请求', 'meaning' => '发送的JSON不是一个有效的请求对象'];
            $response = ['jsonrpc' => '2.0', 'id' => $data['id'], 'result' => null, 'error' => $error];
        } else try {
			//整理参数
			$params = [];
			if(stripos($data['class'], '/')){
				list($params['g'], $params['m']) = explode('/', $data['class']);
			}else{
				$params['m'] = $data['class'];
			}
			$params['a'] = $data['method'];
			$params = array_merge($params, $data['params']);
			
			//声明jsonrpc
			G('jsonrpc', $params);
			
			ob_start();
			//引入初始化文件
			require(CANDYPATH . 'Init.php');
			$contents = ob_get_contents();
			ob_end_clean();
			$response = ['jsonrpc' => '2.0', 'id' => $data['id'], 'result' => $contents];
        } catch (\Exception $e) {
            $error = ['code' => $e->getCode(), 'message' => $e->getMessage()];
            $response = ['jsonrpc' => '2.0', 'id' => $data['id'], 'result' => null, 'error' => $error];
        }
		
		return $connection->send($response);
    }
}
