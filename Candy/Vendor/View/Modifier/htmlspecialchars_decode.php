<?php
function view_modifier_htmlspecialchars_decode(string $string): string
{
    return htmlspecialchars_decode($string);
}
