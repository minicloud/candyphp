<?php
namespace FileMonitor;
use \Workerman\Worker;
use \Workerman\Timer;
/**
 * workerman-filemonitor for windows
 *
 * 监控文件更新并自动reload workerman
 *
 * 使用方法:
 * require_once __DIR__ . '/FileMonitor.php';
 * new FileMonitor($worker, $dir, $timer);
 */
class Monitor
{
    //待监听的项目目录
    private $_monitor_dir = '';
    //热更新间隔时间,默认3s
    private $_interval = 0;
    //最后一次同步时间
    private $_last_time = 0;
    //worker对象
    private $_worker = null;
	
    function __construct ($worker, $dir, $timer = 3)
    {
		// watch files only in daemon mode
		if(Worker::$daemonize && (defined('DEBUG') && DEBUG != 1)){
			//守护进程 和 关闭调试模式下
			$dir = array(
				dirname(CANDYPATH) . '/Source'
			);
		}
		
        // watch Applications catalogue
        $this->_monitor_dir = $dir;
        $this->_interval = $timer;
        $this->_last_time = time();
        $this->_worker = $worker;
        // Emitted when data received
        $worker->reloadable = false;
        // Emitted when data received
        $worker->onWorkerStart = function()
        {
            // watch files only in daemon mode
            Timer::add($this->_interval, [$this, 'monitor']);
        };
    }
    //监听器，kill进程
    public function monitor ()
    {
		$dirs = $this->_monitor_dir;
		$num = count($dirs);
		for($i=0;$i<$num;$i++){
			if(!is_dir($dirs[$i]))	continue;
			// recursive traversal directory
			$dir_iterator = new \RecursiveDirectoryIterator($dirs[$i]);
			$iterator = new \RecursiveIteratorIterator($dir_iterator);
			foreach ($iterator as $file)
			{
				// only check php files
				if(pathinfo($file, PATHINFO_EXTENSION) != 'php' || basename($file) == 'aa512e6773d6745affa5b2ed2592936a.php')
				{
					continue;
				}
				// check mtime
				if ($this->_last_time < $file->getMTime())
				{
					$this->_worker->reloadWorkerForWindows($file.' update and reload.');
					$this->_last_time = $file->getMTime();
					return true;
				}
			}
		}
    }
}
