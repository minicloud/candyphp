<?php
/***
 * Candy框架 Container容器类
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2020-01-29 21:44:56 $   
 */

declare (strict_types = 1);
namespace Candy\Core;
use \ArrayAccess;
use \ReflectionClass;
use \ReflectionMethod;
use \ReflectionException;;
use \ReflectionFunctionAbstract;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');

Class Container implements ArrayAccess {
	/**
     * 容器绑定标识
     * @var array
     */
	protected $bind = [];
	
	/**
     * 容器对象实例
	 *
     * @var Container|Closure
     */
	protected static $instance;
	
	
    /**
     * 容器中的对象实例
     * @var array
     */
    protected $instances = [];

    /**
     * 容器回调
     * @var array
     */
    protected $invokeCallback = [];
	
	
	
	public static function getInstance(): object
    {
        if (is_null(self::$instance)) {
            self::$instance = new static;
        }

        if (self::$instance instanceof Closure) {
            return (self::$instance)();
        }
		
        return self::$instance;
    }

    /**
     * 设置当前容器的实例
     * @access public
     * @param object|Closure $instance
     * @return void
     */
    public static function setInstance($instance): void
    {
        static::$instance = $instance;
    }
	
	/**
     * 创建类的实例 已经存在则直接获取
	 *
     * @access public
     * @param string $abstract    类名或者标识
     * @param array  $vars        变量
     * @param bool   $newInstance 是否每次创建新的实例
     * @return mixed
     */
    public function make(string $abstract, array $vars = [], bool $newInstance = false,array $invokeFunction = []):mixed
    {
        $abstract = $this->getAlias($abstract);
        if (isset($this->instances[$abstract]) && !$newInstance) {
            return $this->instances[$abstract];
        }
		
        if (isset($this->bind[$abstract]) && $this->bind[$abstract] instanceof Closure) {
            $object = $this->invokeFunction($this->bind[$abstract], $vars);
        } else {
            $object = $this->invokeClass($abstract, $vars);
        }
		
		//执行方法
		if (count($invokeFunction) > 0){
			//多个方法
			if (is_array($invokeFunction[0])){
				foreach($invokeFunction as $infunc){
					list($func, $funcVars) = $infunc;
					$object->$func($funcVars);
				}
			}else{
				list($func, $funcVars) = $invokeFunction;
				$object->$func($funcVars);
			}
		}

        if (!$newInstance) {
            $this->instances[$abstract] = $object;
        }

        return $object;
    }
	
	
    /**
     * 删除容器中的对象实例
	 *
     * @access public
     * @param string $name 类名或者标识
     * @return void
     */
    public function delete($name): void
    {
        $name = $this->getAlias($name);

        if (isset($this->instances[$name])) {
            unset($this->instances[$name]);
        }
    }
	
	/**
     * 根据别名获取真实类名
	 *
     * @param  string $abstract
     * @return string
     */
    public function getAlias(string $abstract): string
    {
        if (isset($this->bind[$abstract])) {
            $bind = $this->bind[$abstract];

            if (is_string($bind)) {
                return $this->getAlias($bind);
            }
        }
        return $abstract;
    }
	
    /**
     * 绑定一个类实例到容器
	 *
     * @access public
     * @param string $abstract 类名或者标识
     * @param object $instance 类的实例
     * @return $this
     */
    public function instance(string $abstract,object $instance)
    {
        $abstract = $this->getAlias($abstract);

        $this->instances[$abstract] = $instance;

        return $this;
    }
	
	/**
     * 执行函数或者闭包方法 支持参数调用
	 *
     * @access public
     * @param string|Closure $function 函数或者闭包
     * @param array          $vars     参数
     * @return mixed
     */
    public function invokeFunction(string|Closure $function,array $vars = []):mixed
    {
        try {
            $reflect = new ReflectionFunction($function);
        } catch (Exception $e) {
            Debug::showMessage("function not exists: {$function}()", $function, $e);
        }

        $args = $this->bindParams($reflect, $vars);

        return $function(...$args);
    }
	
	
    /**
     * 调用反射执行类的方法 支持参数绑定
	 *
     * @access public
     * @param mixed $method     方法
     * @param array $vars       参数
     * @param bool  $accessible 设置是否可访问
     * @return mixed
     */
    public function invokeMethod(mixed $method, array $vars = [], bool $accessible = false):mixed
    {
        if (is_array($method)) {
            [$class, $method] = $method;
            $class = is_object($class) ? $class : $this->invokeClass($class);
        } else {
            // 静态方法
            [$class, $method] = explode('::', $method);
        }

        try {
            $reflect = new ReflectionMethod($class, $method);
        } catch (\ReflectionException) {
            $class = is_object($class) ? $class::class : $class;
            throw new Exception('method not exists: ' . $class . '::' . $method . '()', "{$class}::{$method}");
        }

        $args = $this->bindParams($reflect, $vars);

        if ($accessible) {
            $reflect->setAccessible($accessible);
        }

        return $reflect->invokeArgs(is_object($class) ? $class : null, $args);
    }
	
    /**
     * 调用反射执行callable 支持参数绑定
	 *
     * @access public
     * @param mixed $callable
     * @param array $vars       参数
     * @param bool  $accessible 设置是否可访问
     * @return mixed
     */
    public function invoke(mixed $callable, array $vars = [], bool $accessible = false):mixed
    {
        if ($callable instanceof Closure) {
            return $this->invokeFunction($callable, $vars);
        } elseif (is_string($callable) && false == str_contains($callable, '::')) {
            return $this->invokeFunction($callable, $vars);
        } else {
            return $this->invokeMethod($callable, $vars, $accessible);
        }
    }
	
	/**
     * 调用反射执行类的实例化 支持依赖注入
	 *
     * @access public
     * @param string $class 类名
     * @param array  $vars  参数
     * @return mixed
     */
    public function invokeClass(string $class, array $vars = []):mixed
    {
        try {
			$class = Autoload::fullClassName($class);
			$reflect = new ReflectionClass($class);
        } catch (\Exception) {
            Debug::showMessage($class .'类库不存在请检查！', 'PHP');
        }

        if ($reflect->hasMethod('__make')) {
            $method = $reflect->getMethod('__make');
            if ($method->isPublic() && $method->isStatic()) {
                $args = $this->bindParams($method, $vars);
                return $method->invokeArgs(null, $args);
            }
        }
		
		//构造函数
		$constructor = $reflect->getConstructor();
		
		//静态类只返回类名
		if (count($reflect->getMethods(\ReflectionMethod::IS_STATIC)) > 0 && $constructor === null){
			return $class;
		}
        $args = $constructor ? $this->bindParams($constructor, $vars) : [];
		
        $object = $reflect->newInstanceArgs($args);
        $this->invokeAfter($class, $object);
        return $object;
    }
	
	/**
     * 执行invokeClass回调
	 *
     * @access protected
     * @param string $class  对象类名
     * @param object $object 容器对象实例
     * @return void
     */
    protected function invokeAfter(string $class,object $object): void
    {
        if (isset($this->invokeCallback['*'])) {
            foreach ($this->invokeCallback['*'] as $callback) {
                $callback($object, $this);
            }
        }

        if (isset($this->invokeCallback[$class])) {
            foreach ($this->invokeCallback[$class] as $callback) {
                $callback($object, $this);
            }
        }
    }
	
    /**
     * 绑定参数
	 *
     * @access protected
     * @param ReflectionFunctionAbstract $reflect 反射类
     * @param array                      $vars    参数
     * @return array
     */
    protected function bindParams(ReflectionFunctionAbstract $reflect, array $vars = []): array
    {
        if ($reflect->getNumberOfParameters() == 0) {
            return [];
        }

        // 判断数组类型 数字数组时按顺序绑定参数
        reset($vars);
        $type   = key($vars) === 0 ? 1 : 0;
        $params = $reflect->getParameters();
        $args   = [];

        foreach ($params as $param) {
            $name      = $param->getName();
            $lowerName = \Candy\Extend\Str\Str::snake($name);
            $class     = $param->getType();

            if ($class) {
                $args[] = $this->getObjectParam($class->getName(), $vars);
            } elseif (1 == $type && !empty($vars)) {
                $args[] = array_shift($vars);
            } elseif (0 == $type && isset($vars[$name])) {
                $args[] = $vars[$name];
            } elseif (0 == $type && isset($vars[$lowerName])) {
                $args[] = $vars[$lowerName];
            } elseif ($param->isDefaultValueAvailable()) {
                $args[] = $param->getDefaultValue();
            } else {
                Debug::showMessage('method param miss:' . $name);
            }
        }

        return $args;
    }
	
    /**
     * 创建工厂对象实例
	 *
     * @param string $name      工厂类名
     * @param string $namespace 默认命名空间
     * @param array  $args
     * @return mixed
     * @deprecated
     * @access public
     */
    public static function factory(string $name, string $namespace = '', ...$args):mixed
    {
        $class = false !== str_contains($name, '\\') ? $name : $namespace . ucwords($name);

        return Container::getInstance()->invokeClass($class, $args);
    }

    /**
     * 获取对象类型的参数值
	 *
     * @access protected
     * @param string $className 类名
     * @param array  $vars      参数
     * @return mixed
     */
    protected function getObjectParam(string $className, array &$vars):mixed
    {
        $array = $vars;
        $value = array_shift($array);

        if ($value instanceof $className) {
            $result = $value;
            array_shift($vars);
        } else {
            $result = $this->make($className);
        }

        return $result;
    }
	
    /**
     * 获取容器中的对象实例 不存在则创建
	 *
     * @access public
     * @param string     $abstract    类名或者标识
     * @param array|true $vars        变量
     * @param bool       $newInstance 是否每次创建新的实例
     * @return object
     */
    public static function pull(string $abstract, array $vars = [], bool $newInstance = false,array $invokeFunction = []):object
    {
        return self::getInstance()->make($abstract, $vars, $newInstance, $invokeFunction);
    }
	
    /**
     * 获取容器中的对象实例
	 *
     * @access public
     * @param string $abstract 类名或者标识
     * @return object
     */
    public function get(string $abstract):mixed
    {
        if ($this->has($abstract)) {
            return $this->make($abstract);
        }else{
			return null;
		}
    }
	
	/**
     * 绑定一个类、闭包、实例、接口实现到容器
	 *
     * @access public
     * @param string|array $abstract 类标识、接口
     * @param mixed        $concrete 要绑定的类、闭包或者实例
     * @return $this
     */
    public function bind(string|array $abstract,mixed $concrete = null):object
    {
        if (is_array($abstract)) {
            foreach ($abstract as $key => $val) {
                $this->bind($key, $val);
            }
        } elseif ($concrete instanceof Closure) {
            $this->bind[$abstract] = $concrete;
        } elseif (is_object($concrete)) {
            $this->instance($abstract, $concrete);
        } else {
            $abstract = $this->getAlias($abstract);

            $this->bind[$abstract] = $concrete;
        }

        return $this;
    }

    /**
     * @param $name
     * @param $data
     */
    public function setParameter(array $data = []):void
	{
        $this->parameter= [];
        foreach ($data as $key => $value){
            $this->parameter[$key] =$value;
        }
    }
	
    /**
     * 判断容器中是否存在类及标识
	 *
     * @access public
     * @param string $abstract 类名或者标识
     * @return bool
     */
    public function bound(string $abstract): bool
    {
        return isset($this->bind[$abstract]) || isset($this->instances[$abstract]);
    }
	
	/**
     * 判断容器中是否存在类及标识
	 *
     * @access public
     * @param string $name 类名或者标识
     * @return bool
     */
    public function has(string $name): bool
    {
        return $this->bound($name);
    }
	
    /**
     * 判断容器中是否存在对象实例
	 *
     * @access public
     * @param string $abstract 类名或者标识
     * @return bool
     */
    public function exists(string $abstract): bool
    {
        $abstract = $this->getAlias($abstract);

        return isset($this->instances[$abstract]);
    }
	
	public function __set(string $name,mixed $value): void
    {
        $this->bind($name, $value);
    }

    public function __get(string $name):mixed
    {
        return $this->get($name);
    }

    public function __isset(string $name): bool
    {
        return $this->exists($name);
    }

    public function __unset(string $name): void
    {
        $this->delete($name);
    }
	
	public function offsetExists(mixed $key): bool
    {
        return $this->exists($key);
    }

    public function offsetGet(mixed $key): mixed
    {
        return $this->make($key);
    }

    public function offsetSet(mixed $key,mixed $value):void
    {
        $this->bind($key, $value);
    }

    public function offsetUnset(mixed $key):void
    {
        $this->delete($key);
    }

    //Countable
    public function count():int
    {
        return count($this->instances);
    }

    //IteratorAggregate
    public function getIterator():object
    {
        return new ArrayIterator($this->instances);
    }
}
