<?php
/***
 * Candy框架 Model类
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2022-3-5 17:20:22 $   
 */
  
declare(strict_types=1);
namespace Candy\Extend\Console\Command\Make;

use Candy\Extend\Console\Command\Make;

class Model extends Make
{
    protected $type = "Model";

    protected function configure(): void
    {
        parent::configure();
        $this->setName('make:model')
            ->setDescription('Create a new model class.');
    }

    protected function getStub(): string
    {
        return base64_decode('PD9waHAKZGVmaW5lZCgnQ0FORFknKSBPUiBkaWUoJ1lvdSBBcmUgQSBCYWQgR3V5LiBvX08/Pz8nKTsKbmFtZXNwYWNlIHslbmFtZXNwYWNlJX07CgpDbGFzcyB7JWNsYXNzTmFtZSV9CnsKICAgIC8vCn0K');
    }

    protected function getNamespace(string $app): string
    {
        return 'APP\\Model\\' . $app;;
    }
	
	protected function getPathName(string $name): string
    {
		[$base, $model, $app, $path] = explode('\\', $name);
		$name = implode('\\', array_merge([$base], [$app], [$model.'s'], explode('\\', $path)));
        return $this->getBasePath($name) . end(explode('\\', $name)) . '.class.php';
    }
}
