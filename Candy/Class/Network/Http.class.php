<?php
/***
 * Candy框架 HTTP类
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2020-01-23 18:54:01 $   
 */

declare(strict_types=1);
namespace Candy\Extend\Network;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');

final Class Http {
	/**
     * 以GET模拟网络请求
	 *
     * @param string $location HTTP请求地址
     * @param array|string $query GET请求参数
     * @param array $options CURL请求参数
     * @return boolean|string
     */
    public function get(string $location,array|string $query = '',array $options = []): bool|string
    {
        $options['query'] = $query;
        return $this->request('GET', $location, $options);
    }

    /**
     * 以 POST 模拟网络请求
	 *
     * @param string $location HTTP请求地址
     * @param array|string $data POST请求数据
     * @param array $options CURL请求参数
     * @return boolean|string
     */
    public function post(string $location,array|string $data = '',array $options = []): bool|string
    {
        $options['data'] = $data;
        return $this->request('POST', $location, $options);
    }
	
    /**
     * 以 DELETE 模拟网络请求
	 *
     * @param string $location HTTP请求地址
     * @param array|string $data POST请求数据
     * @param array $options CURL请求参数
     * @return boolean|string
     */
    public function delete(string $location,array|string $data = '',array $options = []): bool|string
    {
        $options['data'] = $data;
        return $this->request('DELETE', $location, $options);
    }
	
    /**
     * 以 PUT 模拟网络请求
	 *
     * @param string $location HTTP请求地址
     * @param array|string $data POST请求数据
     * @param array $options CURL请求参数
     * @return boolean|string
     */
    public function put(string $location,array|string $data = '',array $options = []): bool|string
    {
        $options['data'] = $data;
        return $this->request('PUT', $location, $options);
    }
	
    /**
     * 下载远程文件
	 *
     * @param string $url 请求的地址
     * @param string $savePath 本地保存完整路径
     * @param mixed $params 传递的参数
     * @param array $header 传递的头部参数
     * @param int $timeout 超时设置，默认3600秒
     */
    public function down(string $url,string $savePath,mixed $params = '',array $header = [],int $timeout = 3600)
    {
        if (!is_dir(dirname($savePath))) {
            \Candy\Extend\Dir::create(dirname($savePath));
        }
        
        $ch = curl_init();
        $fp = fopen($savePath, 'wb');

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header ? : ['Expect:']);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_NOPROGRESS, 0);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_BUFFERSIZE, 64000);
        curl_setopt($ch, CURLOPT_POST, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $res        = curl_exec($ch);
        $curlInfo   = curl_getinfo($ch);

        if (curl_errno($ch) || $curlInfo['http_code'] != 200) {
            curl_error($ch);
            @unlink($savePath);
            return false;
        } else {
            curl_close($ch);
        }

        fclose($fp);

        return $savePath;
    }
	
    /**
     * 以 FormData 模拟网络请求
	 *
     * @param string $url 模拟请求地址
     * @param array $data 模拟请求参数数据
     * @param array $file 提交文件 [field,name,content]
     * @param array $header 请求头部信息，默认带 Content-type
     * @param string $method 模拟请求的方式 [GET,POST,PUT]
     * @param boolean $returnHeader 是否返回头部信息
     * @return boolean|string
     */
    public function submit(string $url,array $data = [],array $file = [],array $header = [],string $method = 'POST',bool $returnHeader = true): bool|string
    {
        list($boundary, $content) = $this->buildFormData($data, $file);
        $header[] = "Content-type:multipart/form-data;boundary={$boundary}";
        return $this->request($method, $url, ['data' => $content, 'returnHeader' => $returnHeader, 'headers' => $header]);
    }

    /**
     * CURL模拟网络请求
	 *
     * @param string $method 请求方法
     * @param string $location 请求地址
     * @param array $options 请求参数[headers,data,cookie,cookie_file,timeout,returnHeader]
     * @return boolean|string
     */
    public function request(string $method,string $location,array $options = []): bool|string
    {
		//判断是否开启curl
		if(!function_exists('curl_init'))
			Tpl::shutDownTpl('curl_init没有配置，无法使用CURL！');
		
        $curl = curl_init();
        // GET 参数设置
        if (!empty($options['query'])) {
            $location .= (stripos($location, '?') !== false ? '&' : '?') . http_build_query($options['query']);
        }
        // 浏览器代理设置
        curl_setopt($curl, CURLOPT_USERAGENT, $this->getUserAgent());
        // CURL 头信息设置
        if (!empty($options['headers'])) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $options['headers']);
        }
        // Cookie 信息设置
        if (!empty($options['cookie'])) {
            curl_setopt($curl, CURLOPT_COOKIE, $options['cookie']);
        }
        if (!empty($options['cookie_file'])) {
            curl_setopt($curl, CURLOPT_COOKIEJAR, $options['cookie_file']);
            curl_setopt($curl, CURLOPT_COOKIEFILE, $options['cookie_file']);
        }
		//超时时间
        if (!empty($options['timeout'])) {
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $options['timeout']);
            curl_setopt($curl, CURLOPT_TIMEOUT, $options['timeout']);
        }
        // 设置请求方式
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        if (strtolower($method) === 'head') {
            curl_setopt($curl, CURLOPT_NOBODY, 1);
        } elseif (isset($options['data'])) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $this->buildQueryData($options['data']));
        }
        // 请求超时设置
        if (isset($options['timeout']) && is_numeric($options['timeout'])) {
            curl_setopt($curl, CURLOPT_TIMEOUT, $options['timeout']);
        } else {
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        }
        if (empty($options['returnHeader'])) {
            curl_setopt($curl, CURLOPT_HEADER, false);
        } else {
            curl_setopt($curl, CURLOPT_HEADER, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        }
        curl_setopt($curl, CURLOPT_URL, $location);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $content = curl_exec($curl);
        curl_close($curl);
        return $content;
    }

    /**
     * 对 POST 数据过滤处理
	 *
     * @param array $data 需要处理的数据
     * @param boolean $build 是否编译数据
     * @return array|string
     */
    private function buildQueryData(array $data,bool $build = true): array|string
    {
        if (!is_array($data)) return $data;
        foreach ($data as $key => $value) {
            if (is_string($value) && stripos($value, '@') === 0 && class_exists('CURLFile')) {
                if (file_exists($filename = realpath(ltrim($value, '@')))) {
                    list($build, $data[$key]) = [false, new \CURLFile($filename)];
                }
            } elseif ($value instanceof \CURLFile) $build = false;
        }
        return $build ? http_build_query($data) : $data;
    }

    /**
     * 生成 FormData 格式数据内容
	 *
     * @param array $data 表单提交的数据
     * @param array $file 表单上传的文件
     * @return array
     */
    private function buildFormData(array $data = [], array $file = []): array
    {
        list($line, $boundary) = [[], \Candy\Extend\Str\Str::random(18)];
        foreach ($data as $key => $value) {
            $line[] = "--{$boundary}";
            $line[] = "Content-Disposition: form-data; name=\"{$key}\"";
            $line[] = "";
            $line[] = $value;
        }
        if (is_array($file) && isset($file['field']) && isset($file['name'])) {
            $line[] = "--{$boundary}";
            $line[] = "Content-Disposition: form-data; name=\"{$file['field']}\"; filename=\"{$file['name']}\"";
            $line[] = "";
            $line[] = $file['content'];
        }
        $line[] = "--{$boundary}--";
        return [$boundary, join("\r\n", $line)];
    }

    /**
     * 获取浏览器代理信息
	 *
     * @return string
     */
    private function getUserAgent(): string
    {
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            return $_SERVER['HTTP_USER_AGENT'];
        }
        $agents = [
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36 OPR/26.0.1656.60',
			'Opera/8.0 (Windows NT 5.1; U; en)',
			'Mozilla/5.0 (Windows NT 5.1; U; en; rv:1.8.1) Gecko/20061208 Firefox/2.0.0 Opera 9.50',
			'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; en) Opera 9.50',
			'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0',
			'Mozilla/5.0 (X11; U; Linux x86_64; zh-CN; rv:1.9.2.10) Gecko/20100922 Ubuntu/10.10 (maverick) Firefox/3.6.10',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
			'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
			'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.11 TaoBrowser/2.0 Safari/536.11',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.71 Safari/537.1 LBBROWSER',
			'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; LBBROWSER)',
			'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E; LBBROWSER)',
			'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; QQBrowser/7.0.3698.400)',
			'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E)',
			'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 SE 2.X MetaSr 1.0',
			'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SV1; QQDownload 732; .NET4.0C; .NET4.0E; SE 2.X MetaSr 1.0)',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Maxthon/4.4.3.4000 Chrome/30.0.1599.101 Safari/537.36',
			'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 UBrowser/4.0.3214.0 Safari/537.36',
		];
        return $agents[array_rand($agents, 1)];
    }
}