> 本文档最后修订日期：<br>**2022-10-19**

# 前言

## 版本说明
当前文档对应框架版本是<font color="#c7254e">`3.0.1`</font>,如果与您使用的框架版本不符，则有可能本文档的部分或者全部内容将<font color="#c7254e">`不适用`</font>于您正在使用的框架。
在3.x中，我们进行了整体的框架重构，因此与3.x之前的版本不兼容。如果您正在使用2.x或更早的版本，请参看<strong>`升级`</strong>章节的内容。

## 关于workerman 
当前内置workerman的版本为4.1.4，因功能需要已对框架做出多出修改，故暂时无法跟随版本直接更新，涉及的修改点：
* 修改Workerman的Autoloader类  修改加载方法支持加载框架类和方法
* 修改Workerman的Autoloader类  兼容原生类创建
* 修改Workerman的Worker类  增加自定义command命令功能
* 修改Workerman的Http类  缩短缓存键值
* 修改Workerman的Http类  修改gzip配置位置 便于开启gzip
* 修改Workerman的Request类  修改Session方法 可以指定session_id后开启
* 修改Workerman的Request类  新增全局变量服务于框架
* 修改Workerman的Response类  gzip参数
* 修改Workerman的Response类  max-age参数
* 修改Workerman的Response类  staticAgrs参数
* 修改Workerman的Response类  补全httpStatus信息
* 修改Workerman的Response类  获取httpStatus信息方法
* 修改Workerman的Response类  gzip压缩
* 修改Workerman的Response类 添加清除 cookie功能
* 修改Workerman的Session类 兼容框架的缓存驱动

### 在原来webserver的基础上修复的小bug和开发新特性：
* WebServer类  修复目录错误
* WebServer类  去掉默认目录绑定返回502错误
* WebServer类  修改绑定模式支持泛解析
* WebServer类  修改ssl模式添加强制跳转
* WebServer类  添加 $_SERVER['SCRIPT_NAME'] 变量
* WebServer类  支持PATHINFO模式

### 框架内置了组件：
* Channel组件
* GatewayWorker组件
* GlobalData组件
* JsonRpc组件
* WebServer组件
* WebSocket组件

### 其他
* 内置了自动reload 在linux系统内开发 自动更新

更多的信息请查看更新记录。

## 综述
感谢您了解CandyPHP开源框架。

> CandyPHP 是标准的 <strong>`MVC`</strong> 模式框架，理解和使用简单方便，上手迅速。

此框架下2.x开始引入PHP引擎 <strong><font color="#c7254e">`workerman`</font></strong> 实现常规<strong><font color="#c7254e">`Apache`</font></strong>、<strong><font color="#c7254e">`Nginx`</font></strong>模式运行和<strong><font color="#c7254e">`Cli`</font></strong>模式运行并存特性，3.x开始重构优化进一步规范和提升框架速度和便携行。

* 高内聚的可插拔插件和无限的拓展的模块设计
* 简洁清晰的文件结构
* 完善详细的报错信息

标准的<strong>`MVC`</strong> 模式框架，单入口单项目和单入口多项目两种运行方式，功能和模版分离，自动建立模块等诸多特性只为让您的代码书写更流畅，程序运行更稳定。

## 运行环境
运行环境为<font color="#c7254e">`PHP7+`</font>。

## 性能&安全性
CandyPHP秉承一切输入都不可信原则，内置安全类对一切输入进行过滤和检查，DBServer引入PDO、Mysqli等进行参数绑定和检查。内置函数防注入、XSS、CSRF等安全问题。

代码尽量精简和考虑性能优化并且引入<strong><font color="#c7254e">`workerman`</font></strong> 框架可以使用常驻内存的<font color="#c7254e">`Cli`</font></strong>运行模式进一步提升执行效率。

## 交流方式
> CandyPHP官方QQ群：219268056

准备好了吗？接下来，我们将为您详细介绍如何使用CandyPHP框架！

# 目录结构

> 框架目录结构

以下是未初始化之前框架的目录结构
```
/ 根目录
|
├─Candy		框架目录
|   ├─Class		框架扩展类库目录
|   ├─Core		框架核心类库目录
|   ├─Func		框架函数库目录
|   ├─Tpl		框架初始化素材库
|   ├─Vendor		框架第三方扩展类库
|   ├─Autoload.php	框架载入入口
|   └─Version.php	框架版本号
|
├─index.php	默认入口文件
|
└─candys	Cli启动文件
```
> 项目目录结构

以下是项目初始化后的目录结构
```
/ 根目录
|
├─Addons	扩展目录
|   └─Demo	演示扩展目录
|
├─Application   项目目录
|   └─Home	默认项目目录
|      ├─Controls	控制器目录
|      ├─Languages	语言库目录
|      ├─Models		数据模型目录
|      └─Views		默认模版目录(未特殊设定位置时才有)
|
├─Candy		框架目录
|
├─Plugins	插件目录
|   ├─Demo	演示插件目录
|   └─Home	默认项目扩展二开目录
|
├─Public	公共目录
|   ├─css	公共样式目录
|   ├─images	公共图片目录
|   ├─js	公共js目录
|   ├─thumbs	默认缩略图目录
|   └─uploads	默认上传目录
|
├─Runtime	运行缓存目录
|   ├─Cache	缓存目录
|   ├─Comps	模版编译目录
|   ├─Controls	控制器编译目录
|   ├─Data	数据目录
|   ├─Logs	记录目录
|   ├─Models	模型编译目录
|   ├─Tmps	临时缓存目录
|   └─init.lock	初始化锁定文件
|
├─Source	项目资源配置目录
|   ├─Class	项目扩展类库目录
|   ├─Func	项目函数库目录
|   ├─Inc	项目配置目录
|      ├─Addon.inc.php		扩展配置
|      ├─HomeRoute.inc.php	Home路由配置
|      ├─App.inc.php		项目权限菜单配置
|      ├─AttackCode.inc.php	输入过滤配置
|      ├─Command.inc.php	框架扩展命令配置
|      ├─Config.inc.php		基本配置
|      ├─Init.inc.php		初始化配置
|      ├─Plug.inc.php		插件配置
|      ├─Route.inc.php		全局路由配置
|      ├─Sensitive.inc.php	违禁词配置
|      ├─Specialclass.inc.php	特殊类库配置
|      └─System.inc.php		系统配置
|
|   └─Vendor	项目第三方扩展类目录
|
├─Template	默认Home模版目录
|   └─default	前台默认模版目录
|
├─index.php	默认入口文件
|
└─candys	Cli启动文件
```

# 准备工作和核心配置
## 准备工作

> ~~开始之前请确认PHP引擎支持PATHINFO和伪静态。~~ cli模式下自动支持。

> 规则隐藏了index.php文件名，并屏蔽了对/Core、/Source、/Temp等关键路径的访问，以及当HTTP状态码为4xx或5xx时，展示友好的出错页面。同时将流量牵引至/index.php。

## 核心配置
> Apache和Nginx初始化时仅需要对`/Source/Inc/Init.inc.php`内的几项做简单的修改或者直接默认也是可以的。

* `defined('DEBUG') OR define('DEBUG', 1);`

<font color="#ff6600">`DEBUG`</font> <font color="#0099ff">`(Int)`</font> <font color="#bbbbbb">`1`</font> ：
调试模式，<font color="#c7254e">`1`</font> 代表开启、<font color="#c7254e">`0`</font> 代表关闭，默认开启。

当调试模式关闭时，报错等级将调整至最低 **`（意味着某些不重要的错误将会被忽略）`** ，而当调试模式开启时，框架会自动对修改过的模板文件进行编译，并告知浏览器对所有页面都不进行缓存；同时在发生错误时显示详细的报错信息 **`（可能包含文件路径、出错类型或数据库信息）`** 。

在调试模式关闭的情况下，系统会自动开启记录日志的配置项，报错信息将会被写入日志中，日志位置`/Runtime/Logs/error.log`。

* `$tplpath = 'Home@./Template'`;
项目模版文件夹的位置, 值为字符串，@分开 前面为项目名称，后面为路径。自定义时使用，不需要可以注销或删除。

框架默认的模版文件夹路径为：`Application/项目名/Views`

> Cli模式下除了以上的配置 还需要修改service文件配置

`$servers`数组包含了所有的服务器配置，所以只是修改这个数组就可以。其中`webserver`项目配置WEB服务其中`server`为多服务配置每个子数组表示一组服务其中 `count`表示进程数、`gzip`表示gzip等级0表示不开启、`max-age`表示缓存时间、`list`域名绑定信息。

`rpc` RPC服务配置
`globaldata`	全局变量配置
`task`	自动计划任务配置

`把代码解压到根目录，配置完（也可以啥都不做），直接访问就ok了，会自动创建默认的Home项目和初始化所有的目录`

Home是默认应用 也是系统缺省应用 比其他应用多一些特性，比如缺省保护、应用名省略。。。

举例:   

https://domain.com/Admin/Index/index.html 是Admin应用   

https://domain.com/Home/Index/index.html 是Home应用

但是Home一般的样式是这样的  https://domain.com/Index/index.html

## 新应用创建
> 框架新应用的创建也十分简单，不过一定要在 debug模式下哦

举例：

创建一个名称为 User的应用， 在debug模式下 直接输入地址  https://domain.com/User/?add

OK！应用创建完成，所有的应用目录、初始的操作、默认模版和应该的路由配置完全搞定。够不够简单！！！


# 常量

# 扩展类库

# 配置项

# 语法

# 模版

# 缓存及自动编译

# 文件引入

# 路由

# 重名策略

# 报错机制

# 升级

# 安全规范

# 高性能

# 版权声明&联系方式

> 本框架为免费开源、遵循Apache2开源协议的框架，但不得删除框架内文件的版权信息，违者必究。
This framework is free and open source, following the framework of Apache2 open source protocol, but the copyright information of files are not allowed to be deleted,violators will be prosecuted to the maximum extent possible.
©2020 fingerboy. All rights reserved.

> 反馈Bug，可加入 <font color="#c7254e">`QQ群`</font> 或发送邮件至 <font color="#c7254e">`fingerboy@qq.cn`</font> 进行反馈。