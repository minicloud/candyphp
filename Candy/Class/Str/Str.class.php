<?php
/***
 * Candy框架 字符串操作类	
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2020-02-03 20:06:38 $   
 */

declare(strict_types=1);
namespace Candy\Extend\Str;

use \Candy\Core\Tpl;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');

final Class Str {
	protected static $snakeCache = [];
    protected static $camelCache = [];
    protected static $studlyCache = [];

    /**
     * 检查字符串中是否包含某些字符串
	 *
     * @param string       $haystack
     * @param string|array $needles
     * @return bool
     */
    public static function contains(string $haystack,string|array $needles): bool
    {
        foreach ((array) $needles as $needle) {
            if ('' != $needle && mb_strpos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * 检查字符串是否以某些字符串结尾
     *
     * @param  string       $haystack
     * @param  string|array $needles
     * @return bool
     */
    public static function endsWith(string $haystack,string|array $needles): bool
    {
        foreach ((array) $needles as $needle) {
            if (str_ends_with($haystack, (string)$needle)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 检查字符串是否以某些字符串开头
     *
     * @param  string       $haystack
     * @param  string|array $needles
     * @return bool
     */
    public static function startsWith(string $haystack,string|array $needles): bool
    {
        foreach ((array) $needles as $needle) {
            if (str_starts_with($haystack, (string)$needle)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 获取指定长度的随机字母数字组合的字符串
     *
     * @param  int $length
     * @param  int $type 0数字 1数字+小写字母 2数字+字母 3小写字母 4 字母 5 大写字母
     * @param  string $addChars
     * @return string
     */
    public static function random(int $length = 4, int $type = 0, string $addChars = ''): string
    {
        $str = '';
		$_length = strlen($addChars);
		$typelist = [
			[0, 8 + $_length],
			[0, 31 + $_length*2],
			[0, 55 + $_length*3],
			[8 + $_length, 31 + $_length*2],
			[8 + $_length, 55 + $_length*3],
			[31 + $_length*2, 55 + $_length*3]
		];
        switch ($type) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                $chars = '123456789'. $addChars .'abcdefghgkmnpqrstuvwxyz'. $addChars .'ABCDEFGHJKLMNPQRSTUVWXYZ' . $addChars;
				break;
            case 6:
                $chars = "们以我到他会作时要动国产的一是工就年阶义发成部民可出能方进在了不和有大这主中人上为来分生对于学下级地个用同行面说种过命度革而多子后自社加小机也经力线本电高量长党得实家定深法表着水理化争现所二起政三好十战无农使性前等反体合斗路图把结第里正新开论之物从当两些还天资事队批点育重其思与间内去因件日利相由压员气业代全组数果期导平各基或月毛然如应形想制心样干都向变关问比展那它最及外没看治提五解系林者米群头意只明四道马认次文通但条较克又公孔领军流入接席位情运器并飞原油放立题质指建区验活众很教决特此常石强极土少已根共直团统式转别造切九你取西持总料连任志观调七么山程百报更见必真保热委手改管处己将修支识病象几先老光专什六型具示复安带每东增则完风回南广劳轮科北打积车计给节做务被整联步类集号列温装即毫知轴研单色坚据速防史拉世设达尔场织历花受求传口断况采精金界品判参层止边清至万确究书" . $addChars;
				$typelist[] = [0, mb_strlen($chars, 'utf-8') - 1];
				break;
			default : 
				$type = 0;
        }
		for($i=0;$i<$length;$i++){
			$key = rand($typelist[$type][0],$typelist[$type][1]);
			$str .= mb_substr($chars, $key, 1);
		}
        return $str;
    }

    /**
     * 字符串转小写
     *
     * @param  string $value
     * @return string
     */
    public static function lower(string $value): string
    {
        return mb_strtolower($value, 'UTF-8');
    }

    /**
     * 字符串转大写
     *
     * @param  string $value
     * @return string
     */
    public static function upper(string $value): string
    {
        return mb_strtoupper($value, 'UTF-8');
    }

    /**
     * 获取字符串的长度
     *
     * @param  string $value
     * @return int
     */
    public static function length(string $value): int
    {
        return mb_strlen($value);
    }

    /**
     * 截取字符串
     *
     * @param  string   $string
     * @param  int      $start
     * @param  int|null $length
     * @return string
     */
    public static function substr(string $string, int $start, int $length = null): string
    {
        return mb_substr($string, $start, $length, 'UTF-8');
    }

    /**
     * 驼峰转下划线
     *
     * @param  string $value
     * @param  string $first
     * @param  string $delimiter
     * @return string
     */
    public static function snake(string $value,string $first = 'lower', string $delimiter = '_'): string
    {
        $key = $value.$first;

        if (isset(self::$snakeCache[$key][$delimiter])) {
            return self::$snakeCache[$key][$delimiter];
        }

        if (!ctype_lower($value)) {
            $value = preg_replace('/\s+/u', '', $value);
			
			if($first == 'lower'){
				$value = self::lower(preg_replace('/(.)(?=[A-Z])/u', '$1' . $delimiter, $value));
			}else{
				$value = preg_replace('/(.)(?=[A-Z])/u', '$1' . $delimiter, $value);
			}
        }

        return self::$snakeCache[$key][$delimiter] = $value;
    }

    /**
     * 下划线转驼峰(首字母小写)
     *
     * @param  string $value
     * @return string
     */
    public static function camel(string $value): string
    {
        if (isset(self::$camelCache[$value])) {
            return self::$camelCache[$value];
        }

        return self::$camelCache[$value] = lcfirst(self::studly($value));
    }

    /**
     * 下划线转驼峰(首字母大写)
     *
     * @param  string $value
     * @return string
     */
    public static function studly(string $value): string
    {
        if (isset(self::$studlyCache[$value])) {
            return self::$studlyCache[$value];
        }

        $value = ucwords(str_replace(['-', '_'], ' ', $value));

        return self::$studlyCache[$value] = str_replace(' ', '', $value);
    }

    /**
     * 转为首字母大写的标题格式
     *
     * @param  string $value
     * @return string
     */
    public static function title(string $value): string
    {
        return mb_convert_case($value, MB_CASE_TITLE, 'UTF-8');
    }
	
	/**
	 * 唯一数字编码
	 *
	 * @param integer $size
	 * @return 
	 */
	public static function uniqidNumber(bool $md5 = false): string
	{
		$string = md5(memory_get_usage(). self::random() .uniqid('', true).mt_rand(1,99999).$_SERVER['REMOTE_ADDR']);
		if($md5){
			$string=
				'{'.
				substr($string,0,8).'-'.
				substr($string,8,4).'-'.
				substr($string,12,4).'-'.
				substr($string,16,4).'-'.
				substr($string,20,12).
				'}';
		}
		return $string;
	}
	
	/**
	 *
	 * 唯一日期编码
	 * @param integer $size
	 * @return 
	 */
	public static function uniqidDate(int $size = 16): string
	{
		if ($size < 14) $size = 14;
		$string = date('Ymd') . (date('H') + date('i')) . date('s');
		while (strlen($string) < $size) $string .= rand(0, 9);
		return $string;
	}
	
	/**
	 * 敏感词过滤
	 *
     * @param $string
     * @return string
     */
    public static function getSensitive(string $string): array
	{
		empty($list) && $list = loadConfig('Sensitive');
		if(empty($list)){
			self::srtToInc(self::getWebInfo(API_URL . '/Word/getSensitive.shtml'), 'Sensitive');
			empty($list) && $list = loadConfig('Sensitive');
		}
		
		if(empty($list)) return ['content'=>'','msg'=>'敏感词库为空，请检查！'];
		
        $count = 0;
        $sensitiveWord = '';
        $stringAfter = $string;
        $pattern = "/". implode( "|",$list) . "/i";
        if(preg_match_all($pattern, $string, $matches))
		{
            $patternList = $matches[ 0];
            $count = count($patternList);
            $sensitiveWord = implode(',', $patternList);
            $replaceArray = array_combine($patternList,array_fill(0,count($patternList), '*'));
            $stringAfter = strtr($string, $replaceArray);
        }
		
		$msg = '';
        if($count > 0){
			$msg = '匹配到 [' . $count . '] 个敏感词：[' . $sensitiveWord . ']';
        }
        return ['content'=>$stringAfter,'msg'=>$msg,'sensitive'=>$sensitiveWord];
    }
	
	/**
	 * 获取数据
     */
	public static function getWebInfo(string $url): string
	{
		if(empty($url)) return '';
		$words = @file_get_contents($url);
		return $words ?: '';
	} 
	
	/**
	 * 生成配置
     */
	public static function srtToInc($words,string $incName, string $separator = '|'): bool
	{
		if(empty($words) || empty($incName)) return false;
		
		is_string($words) && $words = explode($separator, $words);
		$wordArr = '<?php' . PHP_EOL . 'return [' . PHP_EOL;
		
		is_array($words) && $wordArr .= self::arrToStr($words);
		
		$wordArr .= '];' . PHP_EOL;
		
		$listPath = INCPATH . $incName .'.inc.php';
		file_put_contents($listPath, $wordArr);
		
		return true;
	}
	
	/**
	 * 数组变字符串
     */
	public static function arrToStr(array $arr,int $deth = 0, $tab = "\t", $le = PHP_EOL): string
	{
		$deth++;
		$str = '';
		foreach($arr as $key=>$value){
			$i = $deth;
			if(is_array($value)){
				$nextstr = '';
				while($i>0){
					$nextstr .= $tab;
					$i--;
				}
				$next = self::arrToStr($value, $deth, $tab, $le);
				$str .= $nextstr . (is_int($key) ? '' : '\''. $key .'\'=>') . $nextstr .'[' . $le . $next . $nextstr .'],' . $le;
			}else{
				while($i>0){
					$str .= $tab;
					$i--;
				}
				$str .= (is_int($key) ? '' : '\''. $key .'\'=>') . '\'' . (isset($value) ? trim((string)$value) : '') . '\',' . $le;
			}
		}
		
		return trim ($str, ',');
	}
}
