<?php
/***
 * Candy框架 Validate类
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2022-3-5 17:20:22 $   
 */
  
declare(strict_types=1);
namespace Candy\Extend\Console\Command\Make;

use Candy\Extend\Console\Command\Make;

class Validate extends Make
{
    protected $type = "Validate";

    protected function configure(): void
    {
        parent::configure();
        $this->setName('make:validate')
            ->setDescription('Create a validate.');
    }

    protected function getStub(): string
    {
        return base64_decode('PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiID8+Cjxmb3JtPgoJPGlucHV0IG5hbWU9Im5hbWUiIHR5cGU9InVuaXF1ZSIgbXNnPSJtc2ciIC8+CjwvZm9ybT4=');
    }

    protected function getNamespace(string $app): string
    {
        return 'APP\\Model\\' . $app;;
    }
	
	protected function getPathName(string $name): string
    {
		[$base, $model, $app, $path] = explode('\\', $name);
		$name = implode('\\', array_merge([$base], [$app], [$model.'s'], explode('\\', $path)));
        return $this->getBasePath($name) . end(explode('\\', $name)) . '.xml';
    }
}
