<?php
/***
 * Candy框架 App类
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2022-3-5 17:20:22 $   
 */
  
declare(strict_types=1);
namespace Candy\Extend\Console\Command\Make;

use Candy\Extend\Console\{Command,Input,Output};
use Candy\Extend\Console\Input\{Argument,Option};

class App extends Command
{
	protected $type = "App";
	
	protected function configure(): void
    {
        parent::configure();
        $this->setName('make:app')
			->addOption('addon', '-a', Option::VALUE_NONE, 'The application is a addon')
			->addOption('plugin', '-p', Option::VALUE_NONE, 'The application is a plugin')
            ->addArgument('appName', Argument::REQUIRED, 'The name of the application')
            ->setDescription('Create a new application.');
    }
	
	protected function execute(Input $input, Output $output)
    {
		$appName = strtolower(trim($input->getArgument('appName')));
		
		$pathInfo = $this->getPathName($appName);
		
		
	}
	
	protected function getPathName(string $name): array
    {
		$pathList = [];
		if ($this->input->getOption('addon')) {
            $dirs = [
				'',
			];
        }else if ($this->input->getOption('plugin')) {
            echo 456;
        }else{
			echo 789;
		}
		
		return [];
    }
}
