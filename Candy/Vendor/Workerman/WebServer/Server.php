<?php
namespace WebServer;
use Workerman\Worker;
use Workerman\Protocols\Http\Request;
use Workerman\Protocols\Http\Response;
use Workerman\Protocols\Http\ServerSentEvents;
use Workerman\Connection\TcpConnection;
use Workerman\Connection\AsyncTcpConnection;


/**
 * Web server.
 */
class Server extends Worker
{
    /**
     * Virtual host to path mapping.
     *
     * @var array ['workerman.net'=>'/home', 'www.workerman.net'=>'home/www']
     */
    protected $serverRoot = [];
	
    /**
     * proxy mapping.
     */
    protected $proxy = [];
	
    /**
     * relay mapping.
     */
    protected $relay = [];
	
	/**
     * urlCompelList.
     *
     * @var array
     */
    public static $urlCompelList = [];


    /**
     * Used to save user OnWorkerStart callback settings.
     *
     * @var callable
     */
    protected $_onWorkerStart = null;
	
	/**
     * Add virtual host.
     *
     * @param string $domain
     * @param string $config
     * @return void
     */
    public function addRoot($domain, $config)
    {
        if (\is_string($config)) {
            $config = ['root' => $config];
        }
        $this->serverRoot[$domain] = $config;
    }
	
    /**
     * Construct.
     *
     * @param string $socket_name
     * @param array  $context_option
     */
    public function __construct($socket_name, array $context_option = [])
    {
        list(, $address) = \explode(':', $socket_name, 2);
        parent::__construct('http:' . $address, $context_option);
        $this->name = 'WebServer';
    }

    /**
     * Run webserver instance.
     *
     * @see Workerman.Worker::run()
     */
    public function run()
    {
        $this->_onWorkerStart = $this->onWorkerStart;
        $this->onWorkerStart  = array($this, 'onWorkerStart');
        $this->onMessage      = array($this, 'onMessage');
        parent::run();
    }

    /**
     * Emit when process start.
     *
     * @throws \Exception
     */
    public function onWorkerStart()
    {
        // Try to emit onWorkerStart callback.
        if ($this->_onWorkerStart) {
            try {
                \call_user_func($this->_onWorkerStart, $this);
            } catch (\Exception $e) {
                self::log($e);
                exit(250);
            } catch (\Error $e) {
                self::log($e);
                exit(250);
            }
        }
    }

    /**
     * Emit when http message coming.
     *
     * @param TcpConnection $connection
     * @return void
     */
    public function onMessage(TcpConnection $connection, $request)
    {
		\Candy\Core\Container::getInstance()->bind('Request', $request);
		//Response.
		$response = \Candy\Core\Container::getInstance()->bind('Response', new Response(200))->get('Response');
		
		//microtime.
		$microtime = \microtime(true);
		
		// Init.
		$args = $request->extractArgs();
		$_POST = $args['_POST'];
		$_GET = $args['_GET'];
		$_REQUEST = $args['_REQUEST'];
		$_SERVER = $args['_SERVER'];
		
		// REMOTE_ADDR REMOTE_PORT
        $_SERVER['REMOTE_ADDR'] = $connection->getRemoteIp();
        $_SERVER['REMOTE_PORT'] = $connection->getRemotePort();
		
		// REQUEST_TIME REMOTE_PORT
        $_SERVER['REQUEST_TIME'] = (int)$microtime;
        $_SERVER['REQUEST_TIME_FLOAT'] = $microtime;
		
        $_SERVER['REDIRECT_STATUS'] = 200;
		
		//$_COOKIE
		$_COOKIE = $request->cookie();
		
		//gzip
		if($_SERVER['HTTP_ACCEPT_ENCODING'] && \strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false && isset($connection->worker->gzip)){
			$response->gzip = $connection->worker->gzip;
		}
		
		//Cache-Control
		if(isset($connection->worker->max_age)){
			$response->max_age = $connection->worker->max_age;
		}
		
		// REQUEST_URI.
		$workerman_url_info = \parse_url('http://'. $request->host() . $request->uri());
		if (!$workerman_url_info) {
			$_SERVER['REDIRECT_STATUS'] = 404;
            $response->withStatus(404)->withBody('<h1>400 Bad Request</h1>');
            if (\strtolower($_SERVER['HTTP_CONNECTION']) === "keep-alive") {
                $connection->send($response);
            } else {
                $connection->close($response);
            }
            return;
        }
		
		$workerman_path = isset($workerman_url_info['path']) ? $workerman_url_info['path'] : '/';

        $workerman_path_info      = \pathinfo($workerman_path);
        $workerman_file_extension = isset($workerman_path_info['extension']) ? $workerman_path_info['extension'] : '';
        if ($workerman_file_extension === '') {
            $workerman_path           = ($len = \strlen($workerman_path)) && $workerman_path[$len - 1] === '/' ? $workerman_path . 'index.php' : $workerman_path . '/index.php';
            $workerman_file_extension = 'php';
        }
		
		//泛解析  start
        $workerman_siteConfig = '';
        $server_name = $_SERVER['SERVER_NAME'];
		
		//ACCESS记录
		$log = $_SERVER['REMOTE_ADDR'] . ' - - ['. date('d/M/Y:H:i:s  O') .'] "'. $_SERVER['REQUEST_METHOD'] .' '. $_SERVER['REQUEST_URI'] .' '. $_SERVER['SERVER_PROTOCOL'] .'" {STATUS} {SIZE} "-" "'. $_SERVER['HTTP_USER_AGENT'] ."\"\n";
		$extra = ['.css','.jpg','.jpeg','.gif','.png','.ico','.mp4','.txt','.js','.eot','.svg','.ttf','.woff','.woff2'];
		$rep = ['{STATUS}', '{SIZE}'];
		for($i=0;$i<count($extra);$i++){
			if(stristr($_SERVER['REQUEST_URI'], $extra[$i])){
				$log = '';
				break;
			}
		}
		
		do{
			if(isset($this->serverRoot[$server_name])){
				$workerman_siteConfig = $this->serverRoot[$server_name];
				break;
			}else{
				$server_path = explode('.', $server_name);
				if(count($server_path) > 2){
					$first = array_shift($server_path);
					if($first == '*'){
						array_shift($server_path);
					}
				}
				
				$server_name = '*.'. implode('.', $server_path);
				
				//强制跳转检测 新增此处未跳转的空目录
				if(in_array($server_name, self::$urlCompelList) && $this->transport != 'ssl'){
					$_SERVER['REDIRECT_STATUS'] = 302;
					$response->withStatus(302);
					$response->header('location', 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
					
					//记录log
					if(!empty($log))
						\file_put_contents(LOGPATH.'/access.log', str_replace($rep, [302, 0], $log), FILE_APPEND);
					
					if (\strtolower($_SERVER['HTTP_CONNECTION']) === "keep-alive") {
						$connection->send($response);
					} else {
						$connection->close($response);
					}
					return ;
				}
				
				if(count($server_path) == 2){
					if(isset($this->serverRoot[$server_name])){
						$workerman_siteConfig = $this->serverRoot[$server_name];
					}
					break;
				}
			}
        }while(true);
		
		if(empty($workerman_siteConfig)){
			$_SERVER['REDIRECT_STATUS'] = 502;
			$response->withStatus(502)->withBody('<h1>502 Bad Gateway</h1>');
			//记录log
			if(!empty($log))
				\file_put_contents(LOGPATH.'/access.log', str_replace($rep, [502, 24], $log), FILE_APPEND);
			
			$connection->send($response);
			return;
		}
		
		$workerman_root_dir = rtrim($workerman_siteConfig['root'], '/');
		$workerman_file = $workerman_root_dir . $workerman_path;
		$_SERVER['DOCUMENT_ROOT'] = $workerman_root_dir;
		
		if(isset($workerman_siteConfig['additionHeader'])){
			if(is_string($workerman_siteConfig['additionHeader']))
				$workerman_siteConfig['additionHeader'][] = $workerman_siteConfig['additionHeader'];
			
			foreach($workerman_siteConfig['additionHeader'] as $itionheader){
				[$type, $content] = $itionheader;
				$response->header($type, $content);
			}
		}
		
		$_SERVER['DOCUMENT_URI'] = $workerman_path;
		if ($workerman_file_extension === 'php' && !\is_file($workerman_file)) {
            $workerman_file = "$workerman_root_dir/index.php";
            if (!\is_file($workerman_file)) {
                $workerman_file           = "$workerman_root_dir/index.html";
                $workerman_file_extension = 'html';
            }else{
				$_SERVER['SCRIPT_FILENAME'] =  $workerman_root_dir .'/index.php';
                $_SERVER['SCRIPT_NAME'] = '/index.php';
            }
        }elseif($workerman_file_extension === 'php'){
			$_SERVER['SCRIPT_FILENAME'] = $workerman_file;
			$_SERVER['SCRIPT_NAME'] = $workerman_path;
		}else{
			$_SERVER['SCRIPT_FILENAME'] =  $workerman_root_dir .'/index.php';
			$_SERVER['SCRIPT_NAME'] = '/index.php';
		}
		
		if(\defined('PATHINFO') && $workerman_file_extension != 'php' && !\is_file($workerman_file)){
			$workerman_file = "$workerman_root_dir/index.php";
			$workerman_file_extension = 'php';
		}
		
		// File exsits.
        if (\is_file($workerman_file)) {
			if ((!($workerman_request_realpath = \realpath($workerman_file)) || !($workerman_root_dir_realpath = \realpath($workerman_root_dir))) || 0 !== \strpos($workerman_request_realpath, $workerman_root_dir_realpath)
            ) {
				$_SERVER['REDIRECT_STATUS'] = 404;
                $response->withStatus(404)->withBody('<h1>400 Bad Request</h1>');
				//记录log
				if(!empty($log))
					\file_put_contents(LOGPATH.'/access.log', str_replace($rep, [404, 24], $log), FILE_APPEND);
				
				if (\strtolower($_SERVER['HTTP_CONNECTION']) === "keep-alive") {
					$connection->send($response);
				} else {
					$connection->close($response);
				}
				return;
            }
			
			$workerman_file = \realpath($workerman_file);
			
			// Request php file.
            if ($workerman_file_extension === 'php') {
                $workerman_cwd = \getcwd();
                \chdir($workerman_root_dir);
                \ini_set('display_errors', 'off');
				
				// 如果Accept头是text/event-stream则说明是SSE请求
				if ($request->header('accept') === 'text/event-stream'){
					//记录log
					if(!empty($log))
						\file_put_contents(LOGPATH.'/access.log', str_replace($rep, [200, 1024], $log), FILE_APPEND);
					
					\Candy\Core\Container::getInstance()->bind('Connection', $connection);
					$connection->send(new Response(200, ['Content-Type' => 'text/event-stream']));
					return;
				}
				
                \ob_start();
                // Try to include php file.
                try {
                    // $_SERVER.
                    $_SERVER['REMOTE_ADDR'] = $connection->getRemoteIp();
                    $_SERVER['REMOTE_PORT'] = $connection->getRemotePort();
                    include $workerman_file;
                } catch (\Exception $e) {
                    // Jump_exit?
                    if ($e->getMessage() !== 'jump_exit') {
                        Worker::safeEcho($e);
                    }
                }
				
                $content = \ob_get_clean();
                \ini_set('display_errors', 'on');
				
				//SSE 不正常的请求关闭发包
				if ($request->header('accept') === 'text/event-stream'){
					$connection->close(new ServerSentEvents(['event' => 'message', 'data' => $content, 'id'=>1]));
					return ;
				}
				
				//etag
				$etag = \substr(\md5($content), 8, 16);
				$etag = \substr($etag, 0, 7) . '-' . \substr($etag, 7, 5) . '-' . \substr($etag, 12);
				$etag = 'W/"' . $etag . '"';
				$response->header('ETag', $etag);
				if($etag == trim($_SERVER['HTTP_IF_NONE_MATCH'])){
					$_SERVER['REDIRECT_STATUS'] = 304;
					$response->withStatus(304);
					//记录log
					if(!empty($log))
						\file_put_contents(LOGPATH.'/access.log', str_replace($rep, [304, 0], $log), FILE_APPEND);
				}
				
				if($response->getStatus() == 200){
					$response->withBody($content);
					//记录log
					if(!empty($log))
						\file_put_contents(LOGPATH.'/access.log', str_replace($rep, [200, strlen($content)], $log), FILE_APPEND);
				}
                if (\strtolower($_SERVER['HTTP_CONNECTION']) === "keep-alive") {
                    $connection->send($response);
                } else {
                    $connection->close($response);
                }
                \chdir($workerman_cwd);
                return;
            }
			
			//HTTP_IF_MODIFIED_SINEC 和 HTTP_IF_NONE_MATCH
			if(!empty($_SERVER['HTTP_IF_MODIFIED_SINEC']) && \filemtime($workerman_file) == \strtotime($_SERVER['HTTP_IF_MODIFIED_SINEC'])){
				$_SERVER['REDIRECT_STATUS'] = 304;
				$response->withStatus(304);
				//记录log
				if(!empty($log))
					\file_put_contents(LOGPATH.'/access.log', str_replace($rep, [304, 0], $log), FILE_APPEND);
				
				if (\strtolower($_SERVER['HTTP_CONNECTION']) === "keep-alive") {
                    $connection->send($response);
                } else {
                    $connection->close($response);
                }
				return;
			}
			if(!empty($_SERVER['HTTP_IF_NONE_MATCH'])){
				$etag = \substr(\md5($workerman_file. \filemtime($workerman_file)), 8, 16);
				$etag = \substr($etag, 0, 5) . '-' . \substr($etag, 5, 7) . '-' . \substr($etag, 12);
				$etag = 'W/"' . $etag . '"';
				if($etag == trim($_SERVER['HTTP_IF_NONE_MATCH'])){
					$_SERVER['REDIRECT_STATUS'] = 304;
					$response->withStatus(304);
					//记录log
					if(!empty($log))
						\file_put_contents(LOGPATH.'/access.log', str_replace($rep, [304, 0], $log), FILE_APPEND);
					
					if (\strtolower($_SERVER['HTTP_CONNECTION']) === "keep-alive") {
						$connection->send($response);
					} else {
						$connection->close($response);
					}
					return;
				}
			}

            // Send file to client.
			$response->withFile($workerman_file);
		} else {
            // 404
			if(isset($workerman_siteConfig['custom404']) && \file_exists($workerman_siteConfig['custom404'])){
				$html404 = \file_get_contents($workerman_siteConfig['custom404']);
			}else{
				$html404 = '<html><head><title>404 File not found</title></head><body><center><h3>404 Not Found</h3></center></body></html>';
			}
			$_SERVER['REDIRECT_STATUS'] = 404;
			$response->withStatus(404)->withBody($html404);
			//记录log
			if(!empty($log))
				\file_put_contents(LOGPATH.'/access.log', str_replace($rep, [404, 111], $log), FILE_APPEND);
        }
		
		if (\strtolower($_SERVER['HTTP_CONNECTION']) === "keep-alive") {
            $connection->send($response);
        } else {
            $connection->close($response);
        }
	}
}
