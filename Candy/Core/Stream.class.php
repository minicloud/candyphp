<?php
/***
 * Candy框架 数据流类库
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2019-07-26 11:31:47 $   
 */

declare (strict_types = 1);
namespace Candy\Core;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');

Class Stream {
	private $string;
	private $position;
	
	public function stream_open(string $string, $mode, $options, &$opened_path): bool
	{
		$this->string = str_replace("code://","",$string);
		$this->position = 0;
		return true;
	}
	
	public function stream_read(int $count): string
	{
		$ret = substr($this->string, $this->position, $count);
		$this->position += strlen($ret);
		return $ret;
	}
		
	public function stream_eof(){}
	public function stream_stat(){}
	public function stream_cast(){}
}