<?php
echo '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>系统发生错误</title><style type="text/css">::selection{background-color: #e13300;color: white;}::-moz-selection{background-color: #e13300;color: white;}#full{background-color: #fff;margin: 40px;font: 13px/20px normal helvetica, arial, sans-serif;color: #4f5155;}a{color: #4f5155;background-color: transparent;font-weight: bold;}h1{color: #444;background-color: transparent;border-bottom: 1px solid #d0d0d0;font-size: 19px;font-weight: normal;margin: 0 0 14px 0;padding: 14px 15px 10px 15px;}code{font-family: consolas, monaco, courier new, courier, monospace;font-size: 12px;background-color: #f9f9f9;border: 1px solid #d0d0d0;color: #002166;display: block;margin: 14px 0 14px 0;padding: 12px 10px 12px 10px;}#body{margin: 0 15px 0 15px;}p.footer{text-align: right;font-size: 11px;border-top: 1px solid #d0d0d0;line-height: 32px;padding: 0 10px 0 10px;margin: 20px 0 0 0;}#container{margin: 10px;border: 1px solid #d0d0d0;box-shadow: 0 0 8px #d0d0d0;}</style></head><body id="full"><div id="container"><h1>';
echo empty($type) ? 'PHP' : $type;
if(empty($detailed)){
	echo ' FatalError!</h1><div id="body"><code>'. $msg;
}else{
	echo ' FatalError!</h1><div id="body"><p>Message ： </p><p>' . $msg .'</p><code>';
	echo $detailed ? $detailed : $msg;
}
echo '</code></div><p class="footer">Powered by <a href="'. CANDY_URL .'" target="_blank">CANDYPHP</a> Version <strong>'.CANDY_VERSION.'</strong></p></div>';
echo '</body></html>';
