<?php
function view_modifier_base64_encode(string $string): string
{
    return base64_encode($string);
}
