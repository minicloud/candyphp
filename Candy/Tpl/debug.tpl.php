<?php
namespace Candy\Core;

echo '<style type="text/css">#candy_debug{display:none;margin:0;padding:0;font-size:12px;font-family:"微软雅黑";line-height:20px;text-align:left;border-top:1px solid #ddd;color:#333;background:#fff;position:fixed;_position:absolute;bottom:0;left:0;width:100%;z-index:999999;box-shadow:-2px 2px 20px #555}#candy_open{display:block;height:28px;line-height:28px;border-top-left-radius:3px;z-index:999998;font-family:"微软雅黑";float:right;text-align:right;overflow:hidden;position:fixed;_position:absolute;bottom:0;right:0;background:#232323;color:#fff;font-size:14px;padding:0 8px;cursor:pointer}.tab{padding-left:15px;height:36px;line-height:36px;border-bottom:1px solid #ddd;background-color:#f5f5f5;color:#444}.button{cursor:pointer;float:right;color:#333;padding:0 5px;margin-right:10px}.info{font-size:14px;margin-left:50px}.nav{display:block;width:12%;max-width:100px;float:left;text-align:center;font-size:14px;text-shadow:1px 1px 0 #f2f2f2;cursor:pointer}.mt5{margin-top:5px}.pl20{padding-left:20px}ol{clear:both;margin:0;padding:0 10px;height:200px;overflow:auto}.even{background: #ddd;}</style><div id="candy_debug"><div id="candy_trace_title" class="tab"><span id="candy_close" class="button" style="font-size: 15px;" title="关闭">X</span><span id="candy_min" class="button" title="最小化">—</span><span class="nav">Runtime</span><span class="nav">Framework</span><span class="nav">Request</span><span class="nav">Server</span><span class="nav">Include</span></div><div id="candy_trace_cont"><ol>';
if(isset(Debug::$includefile['app'])){
	echo '<div class="mt5">［自动加载］</div><div class="pl20">';
	foreach(Debug::$includefile['app'] as $file){
		echo '<span>&nbsp;&nbsp;&nbsp;&nbsp;'.$file.'</span>';
	}
	echo '</div>';
}
if(Debug::$application){
	echo '<div class="mt5">［项目信息］</div>';
	echo '<div class="pl20">当前项目：'.$_GET['g'].'、控制器：'.$_GET['m'].'、操作：'.$_GET['a'].'.</div>';
	foreach(Debug::$application as $info){
		echo '<div class="pl20">'.$info.'</div>';
	}
}
if(Debug::$sqls){
	$dbEngine = 'Candy\\Extend\\DB\\Driver\\' . ucfirst(strtolower(G('DRIVER')));
	if($dbEngine::$execute > 0){
		Debug::addMsg('[数据库查询总用时<font color=\'red\'>'. $dbEngine::$totalcate .'</font>ms] (共查询数据：'. $dbEngine::$execute .'次)',2);
		echo '<div class="mt5">［SQL语句］</div>';
		foreach(Debug::$sqls as $sql){
			echo '<div class="pl20">'.$sql.'</div>';
		}
	}
}
echo '</ol><ol style="display:none;">';
if(isset(Debug::$includefile['framework'])){
	echo '<div class="mt5">［自动加载］</div><div class="pl20">';
	foreach(Debug::$includefile['framework'] as $file){
		echo '<span>&nbsp;&nbsp;&nbsp;&nbsp;'.$file.'</span>';
	}
	echo '</div>';
}
if(Debug::$info){
	echo '<div class="mt5">［配置信息］</div>';
	foreach(Debug::$info as $info){
		echo '<div class="pl20">'.$info.'</div>';
	}
}
if(Debug::$paths){
	echo '<div class="mt5">［架构资源］</div>';
	foreach(Debug::$paths as $path){
		echo '<div class="pl20">'.$path.'</div>';
	}
}
echo '<div class="mt5">［其他信息］</div><div class="pl20">框架版本：'.CANDY_VERSION.' <a href="'.CANDY_URL.'" target="_blank" style="color:#888">查看新版</a></div>';
echo '</ol><ol style="display:none;">';
unset($_GET['g']);
unset($_GET['m']);
unset($_GET['a']);
foreach(['GET'=>$_GET,'POST'=>$_POST,'SESSION'=>$_SESSION,'COOKIE'=>$_COOKIE,'SERVER'=>$_SERVER] as $key=>$val){
	if(count($val) > 0){
		echo '<div class="mt5">［'. $key .'］</div>';
		$i = 1;
		foreach($val as $k=>$v){
			echo '<div class="pl20'. ($i%2 == 0 ? ' even' : '') .'"><font color="blue">'. $k .'</font> = <font color="red">'. (is_array($v) ? ('[' . \Candy\Extend\Str\Str::arrToStr($v, 0, null, null) . ']') : $v) .'</font></div>';
			$i++;
		}
	}
}
echo '</ol><ol style="display:none;">';
$spent = Debug::spent();
$sessionid = defined('CLIWORKING') ? \Candy\Core\Container::getInstance()->get('Request')->sessionId() : session_id();
echo '<div class="mt5">［基本信息］</div><div class="pl20">操作系统：'. php_uname('s') .'</div><div class="pl20">服务器时间：'. date('Y-m-d H:i:s', gmTime()) .'</div><div class="pl20">当前网协：' . \getLocalIP() . '/http' . (isSsl() ? 's' : '') .'</div><div class="pl20">运行时间：<font color="red">'. $spent .'ms</font></div><div class="pl20">使用内存：<font color="red">'. usageMemory() .'</font></div><div class="mt5">［引擎信息］</div><div class="pl20">引擎类型：'.$_SERVER['SERVER_SOFTWARE'].'</div><div class="pl20">PHP版本：'. phpversion() .'</div><div class="pl20">POST大小限制：'. ini_get('post_max_size') .'</div><div class="pl20">上传大小限制：'. ini_get('upload_max_filesize') .'</div><div class="pl20">最大执行时间：'. (ini_get('max_execution_time') ?: '无限制') .'</div><div class="pl20">最大内存占用：'. get_cfg_var('memory_limit') .'</div><div class="mt5">［其他信息］</div><div class="pl20">会话ID：'. $sessionid .'</div>';
echo '</ol><ol style="display:none;">';
$i = 1;
$sizeFile = 0;
foreach(requireCache() as $file=>$status){
	if($status && stripos($file, 'Vendor/Workerman/') === false && stripos($file, 'Class/Console/') === false){
		echo '<div class="mt5'. ($i%2 == 0 ? ' even' : '') .'">'. $i .'. '. str_replace(CANDYROOT, '/', $file) .'</div>';
		$sizeFile += filesize($file);
		$i++;
	}
}
echo '<div class="mt5'. ($i%2 == 0 ? ' even' : '') .'">共计加载文件：'. sizeCount($sizeFile) .'</div>';
echo '</ol></div></div><div id="candy_open" title="查看详细">'. $spent .'ms</div><script type="text/javascript">(function(){var e=document.getElementById("candy_open");var j=document.getElementById("candy_close");var c=document.getElementById("candy_min");var a=document.getElementById("candy_debug");var k=document.getElementById("candy_trace_title").getElementsByTagName("span");var f=document.getElementById("candy_trace_cont").getElementsByTagName("ol");var b=document.cookie.match(/candy_trace_page_show=(\d\|\d)/);var g=(b&&typeof b[1]!="undefined"&&b[1].split("|"))||[0,2];var h=function(){document.cookie="candy_trace_page_show="+g.join("|")};for(var d=0;d<k.length;d++){if(d>1){k[d].onclick=(function(l){return function(){for(var i=0;i<f.length;i++){f[i].style.display="none";k[i+2].style.color="#999"}f[l-2].style.display="block";k[l].style.color="#000";g[1]=l;h()}})(d)}}e.onclick=function(){a.style.display="block";e.style.display="none";g[0]=1;h();k[parseInt(g[1])].click()};j.onclick=function(){a.style.display="none";e.style.display="none"};c.onclick=function(){a.style.display="none";e.style.display="block";g[0]=0;h()};e.style.display="block";if(parseInt(g[0])==1){e.click()}})();</script>';
		