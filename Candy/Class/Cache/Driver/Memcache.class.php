<?php
/***
 * Candy框架 内存缓存Memcache类
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2019-08-03 08:31:01 $   
 */
 
declare(strict_types=1);
namespace Candy\Extend\Cache\Driver;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');
 
final Class Memcache {
	private $mc = null;
	private $preFix = 'candy';
	
	/**
	 * 构造方法,用于添加服务器并创建memcahced对象
	 */
	public function __construct(array $servers)
	{
		$mc = new \Memcache;
		//如果有多个memcache服务器
		if(is_array($servers[0])){
			foreach ($servers as $server){
				call_user_func_array([$mc, 'addServer'], $server);
			}
		//如果只有一个memcache服务器
		} else {
			call_user_func_array([$mc, 'addServer'], $servers);
		}
		isset($servers['pre_fix']) && $mc->preFix = $servers['pre_fix'];
		$this->mc=$mc;
	}
	
	/**
	 * 获取memcached对象
	 *
	 * @return	object		memcached对象
	 */
	public function getMem():object
	{
		return $this->mc;
	}
	
	/**
	 * 检查mem是否连接成功
	 *
	 * @return	bool	连接成功返回true,否则返回false
	 */
	public function cacheConnectError(): bool
	{
		$stats=$this->mc->getStats();
		if(empty($stats)){
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * 添加key
	 *
	 * @return	bool	连接成功返回true,否则返回false
	 */
	public function addKey(string $tabName,string $key): bool
	{
		$keys=$this->mc->get($this->preFix . $tabName);
		if(empty($keys)){
			$keys = [];
		}
		//如果key不存在,就添加一个
		if(!in_array($key, $keys)){
			$keys[]=$key;  //将新的key添加到本表的keys中
			$this->mc->set($this->preFix . $tabName, $keys, MEMCACHE_COMPRESSED, 0);
			return true;   //不存在返回true
		}else{
			return false;  //存在返回false
		}
	}
	
	/**
	 * 向memcache中添加数据
	 *
	 * @param	string	$tabName	需要缓存数据表的表名
	 * @param	string	$sql		使用sql作为memcache的key
	 * @param	mixed	$data		需要缓存的数据
	 */
	public function addCache(string $tabName,string $sql,mixed $data): void
	{
		$key=md5($sql);
		//如果不存在
		if($this->addKey($tabName, $key)){
			$this->mc->set($this->preFix . $key, $data, MEMCACHE_COMPRESSED, 0);
		}
	}

	/**
	 * 获取memcahce中保存的数据
	 *
	 * @param	string	$sql	使用SQL的key
	 * @return 	mixed		返回缓存中的数据
	 */
	public function getCache(string $sql): mixed
	{
		$key=md5($sql);
		return $this->mc->get($this->preFix . $key);
	}
	
	/**
	 * 检查key
	 *
	 * @return	bool 
	 */
	public function hasCache(string $tabName,string $key): bool
	{
		$keys=$this->mc->get($tabName);
		if(empty($keys)){
			return false;
		}
		//如果key不存在
		$key=md5($key);
		if(in_array($key, $keys)){
			return true;   //不存在返回true
		}else{
			return false;  //存在返回false
		}
	}

	/**
	 * 删除和同一个表相关的所有缓存
	 *
	 * @param	string	$tabName	数据表的表名
	 */ 
	public function delCache(string $tabName): void
	{
		$keys=$this->mc->get($this->preFix . $tabName);
		//删除同一个表的所有缓存
		if(!empty($keys)){
			foreach($keys as $key){
				$this->mc->delete($this->preFix . $key, 0); //0 表示立刻删除
			}
		}
		//删除表的所有sql的key
		$this->mc->delete($this->preFix . $tabName, 0); 
	}

	/**
	 * 删除单独一个语句的缓存
	 *
	 * @param	string	$sql 执行的SQL语句
	 */
	public function delone(string $sql): void
	{
		$key=md5($sql);
		$this->mc->delete($this->preFix . $key, 0); //0 表示立刻删除
	}
}
