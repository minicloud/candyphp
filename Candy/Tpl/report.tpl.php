<?php
if(G('always200') === null){
	http_response_code($errorCode);
}
$str = '{"code":'.$errorCode.',"status":"error","message":"{$ErrorInfo}","data":{}}';
if(!empty($file)){
	$errorDetail.=' @ Debug#the error in [ '.$file.' ] on [ '.$line.' ].';
}
$errorDetail=str_replace('\\','/',$errorDetail);
$log = '[error] '.$errorDetail.' @ Debug#the error in [ '.$file.' ] on [ '.$line.' ].'.' <'. strval((intval(microtime(true)*1000) - intval(gmTime() * 1000))/1000) ."s>\r\n";
writeLog($log);
$errorDetail = substr(substr(json_encode(array('*'=>$errorDetail),320),6),0,-2);
$str = str_replace('{$ErrorInfo}', $errorDetail, $str);
$str = str_replace('{$ErrorCode}', $ErrorCode, $str);
if(defined('CLIWORKING')){
	headerLoading('Content-Type:application/json; charset=utf-8');
}else{
	header('Content-Type:application/json; charset=utf-8');
}
echo $str;
