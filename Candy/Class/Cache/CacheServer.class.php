<?php
/***
 * Candy框架 数据缓存类	
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2020-02-04 20:00:06 $   
 */
 
declare(strict_types=1);
namespace Candy\Extend\Cache;
use Candy\Core\Container;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');

final Class CacheServer {
	public static $class = null;
	public static $config = null;
	public static $cache_instances = null;
	
	/**
	 * 返回当前终级类对象的实例
	 
	 * @return object
	 */
	public static function instance(): object
	{
		$cache = C('cacheDriver') ?: ['type'=>'File','confing'=>[]];
		$object = 'Candy\\Extend\\Cache\\Driver\\' . ucfirst(strtolower($cache['type']));
		$instance = Container::getInstance()->get($object);
		if(is_null($instance)){
			$instance = Container::getInstance()->bind($object, new $object($cache['confing']))->get($object);
		}
		return $instance;
	}
}
