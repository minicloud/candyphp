<?php
/***
 * Candy框架 View Response
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2019-08-01 23:42:51 $   
 */
 
declare(strict_types=1);
namespace Candy\Core\Respond;

use Candy\Core\Respond;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');

class View extends Respond
{
    /**
     * 输出参数
     * @var array
     */
    protected $options = [];

    /**
     * 输出变量
     * @var array
     */
    protected $vars = [];

    /**
     * 输出过滤
     * @var mixed
     */
    protected $filter;

    /**
     * 输出type
     * @var string
     */
    protected $contentType = 'text/html';

    /**
     * View对象
     * @var BaseView
     */
    protected $view;

    /**
     * 是否内容渲染
     * @var bool
     */
    protected $isContent = false;

    public function __construct($template = null, $code = 200)
    {
        $this->template = $template;
        $this->code = $code;
		
		$objName = C('APPNAME') . C('CONTROL');
		$this->view = \Candy\Core\Container::getInstance()->get($objName) ?? \Candy\Core\Container::getInstance()->get('Action');
    }

    /**
     * 处理数据
     * @access protected
     * @param  mixed $data 要处理的数据
     * @return string
     */
    public function output($data): string
    {
		$this->init($data, $this->code);
		
		$this->view->makeContent($this->template);
        // 渲染模板输出
        $this->view->display();
    }

    /**
     * 模板变量赋值
     * @access public
     * @param  string|array $name  模板变量
     * @param  mixed        $value 变量值
     * @return $this
     */
    public function assign($name, $value = null)
    {
        $this->view->assign($name, $value);

        return $this;
    }

}
