<?php
/***
 * Candy框架 版本文件
 *
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2019-10-01 00:57:19 $
 */

//框架版本号
define('CANDY_VERSION', '4.3.1');
define('CANDY_URL',		'https://framework.icandy.top');
define('BASE_URL',		'https://www.icandy.top');
define('API_URL',		'https://tools.icandy.top');