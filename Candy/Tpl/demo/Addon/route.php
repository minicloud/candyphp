<?php
/**
 *插件路由配置
 **/

return array(
	'URL_MODEL'				=>	1,					//URL访问模式   0  兼容模式  1 统一模式
	'URL_ROUTER_ON'   		=> true,				//开启路由
	//'URL_DEPR'   			=> '-',					//分隔符
	'URL_HTML_SUFFIX'		=>'.shtml',				//伪静态后缀
	'URL_MAP_RULES' 		=> array( 				//静态路由规则
		'show/index'        => 'index/index',
	),
	'URL_ROUTE_RULES' 		=> array( 				//动态路由规则
		'show/:id'        => 'index/index',
	),
);
