<?php
/***
 * Candy框架 初始化文件
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2019-07-24 18:52:59 $   
 */
 
//语法严格模式开启
declare(strict_types=1);

//命名空间
namespace Candy\Core;

//初始化
if(defined('ONCE')){}else{
	//入口标识
	define('CANDY', true);
	
	//分隔符声明
	define('DS', DIRECTORY_SEPARATOR);
	
	//框架目录
	define('CANDYPATH', str_replace("\\", DS, __DIR__ . DS));	//CANDY框架的路径

	//目录声明
	defined('CANDYROOT') OR define('CANDYROOT', dirname(CANDYPATH) . DS);	//项目目录
	define('ADDONPATH', CANDYROOT . 'Addons' . DS);	//扩展目录
	define('PLUGPATH', CANDYROOT . 'Plugins' . DS);	//插件目录
	define('SOURCEPATH', CANDYROOT . 'Source' . DS);	//配置目录
	define('INCPATH', SOURCEPATH . 'Inc' . DS);	//配置目录

	//声明Runtime路径
	if(defined('C_RUN_PATH')){
		define('RUNTIME', rtrim(C_RUN_PATH, DS). DS);		
	}else{
		define('RUNTIME', CANDYROOT . 'Runtime' . DS);
	}

	define('LOGPATH', RUNTIME . 'Logs' . DS);	//log目录

	//包含框架中的函数库文件
	require(CANDYPATH.'Func/global.func.php');

	//框架版本
	requireCache(CANDYPATH.'Version.php');

	//加载自动类库
	requireCache(CANDYPATH .'Core/Autoload.class.php');

	//框架自动加载类
	Autoload::loading();
	
	//一次初始标识 用于兼容workerman
	if(preg_match("/cli/i", php_sapi_name())){
		define('ONCE', true);
	}
	
	//包含Debug和Exception处理类
	if(defined('CLIWORKING')){
		Debug::reload();
	}else{
		Debug::start();
	}
}