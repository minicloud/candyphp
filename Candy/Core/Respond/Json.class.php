<?php
/***
 * Candy框架 Html Response
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2019-08-01 23:42:51 $   
 */
 
declare(strict_types=1);
namespace Candy\Core\Respond;

use Candy\Core\Respond;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');

class Json extends Respond
{
    /**
     * 输出type
     * @var string
     */
    protected $contentType = 'application/json';

    public function __construct($data = '', $code = 200)
    {
        $this->init($data, $code);
		$data = $this->output('');
		if(defined('CLIWORKING')){
			headerLoading('Content-Type:application/json; charset=utf-8');
		}else{
			header('Content-Type:application/json; charset=utf-8');
		}
		$this->content = json($data);
    }
}
