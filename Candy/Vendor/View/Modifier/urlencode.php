<?php
function view_modifier_urlencode(string $string): string
{
    return urlencode($string);
}
