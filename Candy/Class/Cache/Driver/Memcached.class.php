<?php
/***
 * Candy框架 内存缓存Memcached类
 * 
 * $Author: 刘森 (fingerboy@qq.com) $
 * $Date: 2019-08-03 08:31:01 $   
 */
 
declare(strict_types=1);
namespace Candy\Extend\Cache\Driver;

defined('CANDY') OR die('You Are A Bad Guy. o_O???');
 
final Class Memcached {
	private $mcd = null;
	private $preFix = 'asdf';
	private $persistentId = 'ASDF';
	private $options;
	
	/**
	 * 构造方法,用于添加服务器并创建memcahced对象
	 */
	public function __construct(array $servers)
	{
		$mc = new \Memcached;
		
		if(is_array($servers[0])){
			//如果有多个memcached服务器
			call_user_func_array([$mc, 'addServers '], $servers);
		} else {
			//如果只有一个memcached服务器
			call_user_func_array([$mc, 'addServer'], $servers);
		}
		
		//配置
        $preFix = isset($servers['pre_fix']) ? $servers['pre_fix'] : $this->preFix;
        $mc->setOptions([
                \Memcached::OPT_PREFIX_KEY  => $preFix,
                \Memcached::OPT_COMPRESSION => false
            ]
        );
		
		$this->mc=$mc;
	}
	
	/**
	 * 获取memcached对象
	 *
	 * @return	object		memcached对象
	 */
	public function getMem() :object
	{
		return $this->mc;
	}
	
	/**
	 * 检查mem是否连接成功
	 *
	 * @return	bool	连接成功返回true,否则返回false
	 */
	public function cacheConnectError(): bool
	{
		$stats=$this->mc->getStats();
		if(empty($stats)){
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * 添加key
	 *
	 * @return	bool	连接成功返回true,否则返回false
	 */
	public function addKey(string $tabName,string $key): bool
	{
		$keys=$this->mc->get($tabName);
		if(empty($keys)){
			$keys = [];
		}
		//如果key不存在,就添加一个
		if(!in_array($key, $keys)){
			$keys[]=$key;  //将新的key添加到本表的keys中
			$this->mc->set($tabName, $keys);
			return true;   //不存在返回true
		}else{
			return false;  //存在返回false
		}
	}
	
	/**
	 * 向memcache中添加数据
	 *
	 * @param	string	$tabName	需要缓存数据表的表名
	 * @param	string	$sql		使用sql作为memcache的key
	 * @param	mixed	$data		需要缓存的数据
	 */
	public function addCache(string $tabName,string $sql,mixed $data): void
	{
		$key=md5($sql);
		//如果不存在
		if($this->addKey($tabName, $key)){
			$this->mc->set($key, $data);
		}
	}

	/**
	 * 获取memcahce中保存的数据
	 *
	 * @param	string	$sql	使用SQL的key
	 * @return 	mixed		返回缓存中的数据
	 */
	public function getCache(string $sql): mixed
	{
		$key=md5($sql);
		return $this->mc->get($key);
	}
	
	/**
	 * 检查key
	 *
	 * @return	bool 
	 */
	public function hasCache(string $tabName,string $key): bool
	{
		$keys=$this->mc->get($tabName);
		if(empty($keys)){
			return false;
		}
		//如果key不存在
		$key=md5($key);
		if(in_array($key, $keys)){
			return true;   //不存在返回true
		}else{
			return false;  //存在返回false
		}
	}

	/**
	 * 删除和同一个表相关的所有缓存
	 *
	 * @param	string	$tabName	数据表的表名
	 */ 
	public function delCache(string $tabName): void
	{
		$keys=$this->mc->get($tabName);
		//删除同一个表的所有缓存
		if(!empty($keys)){
			foreach($keys as $key){
				$this->mc->delete($key, 0); //0 表示立刻删除
			}
		}
		//删除表的所有sql的key
		$this->mc->delete($tabName, 0); 
	}

	/**
	 * 删除单独一个语句的缓存
	 *
	 * @param	string	$sql 执行的SQL语句
	 */
	public function delone(string $sql): void
	{
		$key=md5($sql);
		$this->mc->delete($key, 0); //0 表示立刻删除
	}
}
